package com.example.amir.angular;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.dd.CircularProgressButton;
import com.github.library.bubbleview.BubbleTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Amir on 12/31/2016.
 */

public class RoundFragment extends Fragment {
    private static String PARCELABLE_ITEM = "parcel_item";
    private RoundItem roundItem;
    private View root;
    private Spinner group1EditBox;
    private Spinner group2EditBox;

    private EditText changeDurationEditBox;
    private RoundFragDelegate roundFragDelegate;
    private Button saveButton;
    private CircularProgressButton startButton;
    private Button backButton;
    private Button saveObstacleButton;
    private TextView cronometer;
    private Spinner obstacle1Spinner;
    private Spinner obstacle2Spinner;
    private Spinner obstacle3Spinner;
    private Spinner obstacle4Spinner;

    private TextView leftTopPoints;
    private TextView leftBottomPoints;
    private TextView rightTopPoints;
    private TextView rightBottomPoints;


    private AutofitTextView group1Name;
    private AutofitTextView group2Name;

    private ElegantNumberButton leftTop;
    private ElegantNumberButton leftBottom;
    private ElegantNumberButton rightTop;
    private ElegantNumberButton rightBottom;

    private ElegantNumberButton leftBonus;
    private ElegantNumberButton rightBonus;


    private RelativeLayout relativeLayout;

    private CardView pointsCardView;
    private CardView obstacleCard;
    private CardView editRoundCard;


    private ObstacleItem obstacleItem1;
    private ObstacleItem obstacleItem2;
    private ObstacleItem obstacleItem3;
    private ObstacleItem obstacleItem4;

    private Integer current1ValueResult1 = 0;
    private Integer current1ValueResult2 = 0;
    private Integer current2ValueResult1 = 0;
    private Integer current2ValueResult2 = 0;

    private boolean wasLastGroup1 = true;
    private boolean wasLastObs1 = true;
    private boolean wasLastPositive = true;
    private boolean updatePointsInAction = false;
    private Toast toastObject;
    private BubbleTextView bubbleTextVew;

    private CountDownTimer countDownTimer2;
    private CountDownTimer countDownTimer1;
    private RelativeLayout leftPointsRelative;
    private RelativeLayout rightPointsRelative;
    private Button resetButton;
    private boolean isPerma;

    public int getMaxDoneValue() {
        return maxDoneValue;
    }

    public void setMaxDoneValue(int maxDoneValue) {
        if (this.maxDoneValue<maxDoneValue)
            this.maxDoneValue = maxDoneValue;
    }

    private int maxDoneValue = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        roundFragDelegate = (RoundFragDelegate)context;
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetach() {
        roundFragDelegate = null;
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }

    public String getRoundItemId(){
        if (roundItem==null){
            return "";
        }
        return roundItem.get_id();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setStatus(RoundStatusEvent event){
        if ((event.get_id()).equals(roundItem.get_id())){
            bindRound(event.getStatus(),event.getCounterEnds(),event.getObsType(),event.getObs1tick(),event.getObs2tick(),event.getObs3tick(),event.getObs4tick());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setPoints(PointsStatusEvent event){
        Log.d("fragment", "setPoints: reached destination");
        if ((event.get_id()).equals(roundItem.get_id())){
            Log.d("fragment", "setPoints: passed identification");
            leftTop.setNumber(event.getLeftobs1().toString());
            leftBottom.setNumber(event.getLeftobs2().toString());

            leftBonus.setNumber(event.getLeftBonus().toString());

            rightTop.setNumber(event.getRightobs1().toString());
            rightBottom.setNumber(event.getRightobs2().toString());

            rightBonus.setNumber(event.getRightBonus().toString());
        }
    }


    private void showTalkbackDurationTooltip (String data) {
        if ( bubbleTextVew != null && bubbleTextVew.getVisibility () != View.VISIBLE ) {
            bubbleTextVew.setText (data);
            bubbleTextVew.setVisibility ( View.VISIBLE );
            AnimHelper.scaleInOut ( bubbleTextVew, 300, 0.15f );
            bubbleTextVew.postDelayed ( new Runnable () {
                @Override
                public void run ( ) {
                    bubbleTextVew.setVisibility ( View.GONE );
                }
            }, 1000 );
        }
    }


    public boolean isRoundRestarted(){
        if (roundItem.getIsDone() < maxDoneValue){
            return true;
        }
        return false;
    }

    private void bindRound(int status, Integer counterEnds,int obsType,Integer obs1tick,Integer obs2tick,Integer obs3tick,Integer obs4tick) {
        if (status == 4) {
            if (countDownTimer2 != null) {
                countDownTimer2.cancel();
            }
            if (countDownTimer1 != null) {
                countDownTimer1.cancel();
            }

            cronometer.setText("");
            startButton.setIdleText("Complete");
            startButton.setProgress(0);
            startButton.setCompleteText("Complete");
            startButton.setProgress(100);
            relativeLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.color8));
            roundItem.setIsDone(4);
            setMaxDoneValue(4);
            /*
            if (Config.AdminStatus>0 && Config.AdminStatus<3) {
                if (toastObject != null) {
                    toastObject.cancel();
                }
                toastObject = Toast.makeText(getContext(),"supposed to close frag",Toast.LENGTH_SHORT);
                toastObject.show();

            }
            */
        }else if (status == 3){
            if (Config.AdminStatus == 0){
                cronometer.setVisibility(View.GONE);
                countDownTimer2.cancel();
                startButton.setCompleteText("Approve");
                startButton.setProgress(100);
            }else{
                startButton.setCompleteText("Complete");
            }
            roundItem.setIsDone(3);
            setMaxDoneValue(3);
            relativeLayout.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.color7));
        }else if (status == 1){
            startButton.setProgress(0);
            startButton.setIdleText("Start Round");
            relativeLayout.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.color6));
            Log.d("RoundFragment", "maybe?: restarted? "+roundItem.getIsDone()+"  max was: "+ getMaxDoneValue());
            roundItem.setIsDone(1);
            setMaxDoneValue(1);
            if (getMaxDoneValue()>roundItem.getIsDone()){
                Log.d("RoundFragment", "refresh page");
                PermaRoundFragmentDelegate t = new PermaRoundFragmentDelegate(roundItem);
                EventBus.getDefault().post(t);
            }
        }else if (status == 2){
            obstacleItem1.setTickPoints(obs1tick.toString());
            obstacleItem2.setTickPoints(obs2tick.toString());
            obstacleItem3.setTickPoints(obs3tick.toString());
            obstacleItem4.setTickPoints(obs4tick.toString());
            startButton.setCompleteText("Finish Match");
            startButton.setProgress(0);
            setMaxDoneValue(2);
            roundItem.setRoundTime(counterEnds);

            if (obstacleCard != null){
                obstacleCard.setVisibility(View.GONE);
            }
            if (editRoundCard != null){
                editRoundCard.setVisibility(View.GONE);
            }
            if (pointsCardView != null){
                pointsCardView.setVisibility(View.VISIBLE);
            }


            if (obsType%10 == 1 ){
                leftTop.setVisibility(View.INVISIBLE);
            }else{
                leftTop.setVisibility(View.VISIBLE);
            }

            if ((obsType/10)%10 == 1 ){
                leftBottom.setVisibility(View.INVISIBLE);
            }else{
                leftBottom.setVisibility(View.VISIBLE);
            }

            if ((obsType/100)%10 == 1 ){
                rightTop.setVisibility(View.INVISIBLE);
            }else{
                rightTop.setVisibility(View.VISIBLE);
            }

            if ((obsType/1000)%10 == 1 ){
                rightBottom.setVisibility(View.INVISIBLE);
            }else{
                rightBottom.setVisibility(View.VISIBLE);
            }
            activateMatchProgress(roundItem.getRoundTime()*60*1000);
            roundItem.setIsDone(2);
        }

    }

    /*
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void notifyChangeInRoundItem(UpdateRoundDelegate event) {
        if (event.getDocumentID().equals(roundItem.get_id())) {
            event.getUpdatedValuesJson()
            bindRound(event.getUpdatedValuesJson().getInt("status"), Integer counterEnds,int obsType, Integer obs1tick, Integer
            obs2tick, Integer obs3tick, Integer obs4tick);

        }
    }
    */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            roundItem = bundle.getParcelable(PARCELABLE_ITEM);

            if (roundItem != null) {
                maxDoneValue = roundItem.getIsDone();
            }

            isPerma = bundle.getBoolean("isPerma");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.round_fragment, container, false);
        //appbar2 = (AppBarLayout)container.findViewById(R.id.appbar2);
        initViews(root);
        bindData();
        initListeners();
        return root;
    }

    private void bindObstacles(){
        for(RecyclerItem recyclerItem : Config.obstacleList) {
            if (recyclerItem instanceof ObstacleItem) {
                ObstacleItem obstacleItem = (ObstacleItem) recyclerItem;
                if ((roundItem.getObstacleItem1Name()).equals(obstacleItem.getName())){
                    obstacleItem1 = obstacleItem;
                }
                if ((roundItem.getObstacleItem2Name()).equals(obstacleItem.getName())){
                    obstacleItem2 = obstacleItem;
                }
                if ((roundItem.getObstacleItem3Name()).equals(obstacleItem.getName())){
                    obstacleItem3 = obstacleItem;
                }
                if ((roundItem.getObstacleItem4Name()).equals(obstacleItem.getName())){
                    obstacleItem4 = obstacleItem;
                }
            }
        }
        if (obstacleItem1 == null){
            obstacleItem1 = new ObstacleItem();
            obstacleItem1.set_id("0");
            obstacleItem1.setName("none");
            obstacleItem1.setDescription("none");
            obstacleItem1.setOwner("none");
            obstacleItem1.setMaxPoints("0");
            obstacleItem1.setTickPoints(roundItem.getObs1tick().toString());
        }
        if (obstacleItem2 == null){
            obstacleItem2 = new ObstacleItem();
            obstacleItem2.set_id("0");
            obstacleItem2.setName("none");
            obstacleItem2.setDescription("none");
            obstacleItem2.setOwner("none");
            obstacleItem2.setMaxPoints("0");
            obstacleItem2.setTickPoints(roundItem.getObs2tick().toString());
        }
        if (obstacleItem3 == null){
            obstacleItem3 = new ObstacleItem();
            obstacleItem3.set_id("0");
            obstacleItem3.setName("none");
            obstacleItem3.setDescription("none");
            obstacleItem3.setOwner("none");
            obstacleItem3.setMaxPoints("0");
            obstacleItem3.setTickPoints(roundItem.getObs3tick().toString());
        }
        if (obstacleItem4 == null){
            obstacleItem4 = new ObstacleItem();
            obstacleItem4.set_id("0");
            obstacleItem4.setName("none");
            obstacleItem4.setDescription("none");
            obstacleItem4.setOwner("none");
            obstacleItem4.setMaxPoints("0");
            obstacleItem4.setTickPoints(roundItem.getObs4tick().toString());
        }
    }



    private void initViews(View v) {


        //appbar2.setVisibility(View.VISIBLE);
        group1EditBox = (Spinner)v.findViewById(R.id.editText1);
        group2EditBox = (Spinner)v.findViewById(R.id.editText2);

        changeDurationEditBox = (EditText)v.findViewById(R.id.editTime);


        relativeLayout = (RelativeLayout)v.findViewById(R.id.frag_relative);



        leftTopPoints = (TextView)v.findViewById(R.id.group1_obs1_points);
        leftBottomPoints = (TextView)v.findViewById(R.id.group1_obs2_points);

        rightTopPoints = (TextView)v.findViewById(R.id.group2_obs1_points);
        rightBottomPoints = (TextView)v.findViewById(R.id.group2_obs2_points);

        pointsCardView = (CardView)v.findViewById(R.id.point_cardview);
        editRoundCard = (CardView)v.findViewById(R.id.cardView);
        obstacleCard = (CardView)v.findViewById(R.id.obstaclecardView);

        leftPointsRelative = (RelativeLayout)v.findViewById(R.id.left_group_points);
        rightPointsRelative = (RelativeLayout)v.findViewById(R.id.right_group_points);

        //leftTopUp = (Button) v.findViewById(R.id.step1_pass_1);
        //leftTopDown = (Button) v.findViewById(R.id.step1_pass_n1);

        leftTop = (ElegantNumberButton) v.findViewById(R.id.step1_pass_1);
        leftBottom = (ElegantNumberButton) v.findViewById(R.id.step1_pass_2);

        rightTop = (ElegantNumberButton) v.findViewById(R.id.step2_pass_n1);

        rightBottom = (ElegantNumberButton) v.findViewById(R.id.step2_pass_n2);



        leftBonus = (ElegantNumberButton) v.findViewById(R.id.left_bonus);
        rightBonus = (ElegantNumberButton) v.findViewById(R.id.right_bonus);
        bubbleTextVew = (BubbleTextView) v.findViewById(R.id.bubble);


        cronometer = (TextView)v.findViewById(R.id.chronometer);


        group1Name = (AutofitTextView)v.findViewById(R.id.leftGroupPoint);
        group2Name = (AutofitTextView)v.findViewById(R.id.rightGroupPoint);


        saveObstacleButton = (Button) v.findViewById(R.id.button_save_obstacle);
        resetButton = (Button) v.findViewById(R.id.reset_button);




        obstacle1Spinner = (Spinner)v.findViewById(R.id.spinner1);
        obstacle2Spinner = (Spinner)v.findViewById(R.id.spinner2);
        obstacle3Spinner = (Spinner)v.findViewById(R.id.spinner3);
        obstacle4Spinner = (Spinner)v.findViewById(R.id.spinner4);

        saveButton = (Button)v.findViewById(R.id.button_save);
        startButton = (CircularProgressButton)v.findViewById(R.id.button_start);

        backButton = (Button)v.findViewById(R.id.back_button);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private ArrayAdapter<String> getAdapterForSpinner(ArrayList<RecyclerItem> recyclerItems,String obstacle, int type){
        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("none");
        for(RecyclerItem recyclerItem : recyclerItems){
            String testString = recyclerItem.getOfficialName();
            if (testString != null){
                if (obstacle.equals(testString)){
                    stringList.add(0,testString);
                }else
                stringList.add(testString);
            }
        }

        ArrayAdapter<String> spinnerArrayAdapter;
        if (type ==1){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.red_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.red_spinner_dropdown);
        }else if (type ==2){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.blue_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.blue_spinner_dropdown);
        }else {
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, stringList);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }
        return spinnerArrayAdapter;
    }

    private ArrayAdapter<String> getAdapterForSpinnerNoNull(ArrayList<RecyclerItem> recyclerItems,String selected,int type){
        ArrayList<String> stringList = new ArrayList<>();
        if (selected.equals("none")){
            stringList.add(0,selected);
        }
        for(RecyclerItem recyclerItem : recyclerItems){
            String testString = recyclerItem.getOfficialName();
            if (testString != null){
                if (selected.equals(recyclerItem.getOfficialName())){
                    stringList.add(0,recyclerItem.getOfficialName());
                }else
                    stringList.add(recyclerItem.getOfficialName());
            }
        }
        ArrayAdapter<String> spinnerArrayAdapter;
        if (type ==1){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.red_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.red_spinner_dropdown);
        }else if (type ==2){
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.blue_spinner, stringList);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.blue_spinner_dropdown);
        }else {
            spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, stringList);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }
        return spinnerArrayAdapter;
    }
    private ArrayAdapter<String> getAdapterForSpinnerNoNull(ArrayList<RecyclerItem> recyclerItems,long selected){
        ArrayList<String> stringList = new ArrayList<>();
        for(RecyclerItem recyclerItem : recyclerItems){
            if (recyclerItem instanceof  RoundItem){
                RoundItem round =  (RoundItem)recyclerItem;
                if (round.getRoundTime() == selected){
                    stringList.add(0,Long.toString(round.getRoundTime()));
                }else
                    stringList.add(Long.toString(round.getRoundTime()));
            }
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, stringList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        return spinnerArrayAdapter;
    }

    private void bindData(){
        if (roundItem != null){
            bindObstacles();

            if (Config.AdminStatus == 1){
                rightPointsRelative.setVisibility(View.GONE);
            }else if(Config.AdminStatus == 2){
                leftPointsRelative.setVisibility(View.GONE);
            }else if(Config.AdminStatus == 3){
                leftTop.removeButtons();
                leftBottom.removeButtons();
                rightTop.removeButtons();
                rightBottom.removeButtons();

                leftBonus.removeButtons();
                rightBonus.removeButtons();

            }
            if (Config.AdminStatus>0){
                if (roundItem.getIsDone()<3){
                    startButton.setProgress(1);
                    startButton.setIdleText("Awaiting Start");
                    startButton.setProgress(0);
                }
                resetButton.setVisibility(View.GONE);
            }

            group1EditBox.setAdapter(getAdapterForSpinnerNoNull(Config.groupsList,roundItem.getGroup1(),1));
            group1EditBox.setSelection(0);

            group2EditBox.setAdapter(getAdapterForSpinnerNoNull(Config.groupsList,roundItem.getGroup2(),2));
            group2EditBox.setSelection(0);

            changeDurationEditBox.setText(roundItem.getRoundTime().toString());



            obstacle1Spinner.setAdapter( getAdapterForSpinner(Config.obstacleList,roundItem.getObstacleItem1Name(),1));
            obstacle2Spinner.setAdapter( getAdapterForSpinner(Config.obstacleList,roundItem.getObstacleItem2Name(),1));
            obstacle3Spinner.setAdapter( getAdapterForSpinner(Config.obstacleList,roundItem.getObstacleItem3Name(),2));
            obstacle4Spinner.setAdapter( getAdapterForSpinner(Config.obstacleList,roundItem.getObstacleItem4Name(),2));

            leftTop.setAutoChangeOnClick(false);
            leftBottom.setAutoChangeOnClick(false);
            rightTop.setAutoChangeOnClick(false);
            rightBottom.setAutoChangeOnClick(false);

            leftBonus.setAutoChangeOnClick(false);
            rightBonus.setAutoChangeOnClick(false);






            leftTop.setChangeValue(obstacleItem1.getTickPointsAsInt());
            leftBottom.setChangeValue(obstacleItem2.getTickPointsAsInt());
            rightTop.setChangeValue(obstacleItem1.getTickPointsAsInt());
            rightBottom.setChangeValue(obstacleItem2.getTickPointsAsInt());

            leftBonus.setChangeValue(2);
            rightBonus.setChangeValue(2);





            leftTop.setNumber(roundItem.getObs1Points().toString());
            leftBottom.setNumber(roundItem.getObs2Points().toString());
            rightTop.setNumber(roundItem.getObs3Points().toString());
            rightBottom.setNumber(roundItem.getObs4Points().toString());

            leftBonus.setNumber(roundItem.getLeftBonusPoints().toString());
            rightBonus.setNumber(roundItem.getRightBonusPoints().toString());






            //saveButton.setText("Delete Round");
            startButton.setEnabled(true);
            if (Config.AdminStatus > 0){startButton.setIdleText("Awaiting start");}
            startButton.setProgress(0);
            if ((roundItem.getIsDone()==3) && (Config.AdminStatus == 0)){
                startButton.setProgress(100);
                relativeLayout.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.color7));
                if (obstacleCard != null){
                    obstacleCard.setVisibility(View.GONE);
                }
                if (editRoundCard != null){
                    editRoundCard.setVisibility(View.GONE);
                }
                if (pointsCardView != null){
                    pointsCardView.setVisibility(View.VISIBLE);
                }
            }else if ((roundItem.getIsDone() == 2)|| (roundItem.getIsDone()==3) && (Config.AdminStatus > 0)){
                //Log.d("Adapter", "bindData: 3 1: " + roundItem.getMatchEnd());
                //Log.d("Adapter", "bindData: 3 2: " + System.currentTimeMillis());
                //Log.d("Adapter", "bindData: 3 3: " + Config.serverDifTime);
                int remainingSecs = (int)(roundItem.getMatchEnd()-System.currentTimeMillis());
                //Log.d("Adapter", "bindData: 3 4: " + remainingSecs);
                pointsCardView.setVisibility(View.GONE);
                //if (toastObject != null) {
                //    toastObject.cancel();
                //}
                //toastObject = Toast.makeText(getContext(),"time left: "+remainingSecs,Toast.LENGTH_SHORT);
                //toastObject.show();
                if (remainingSecs > 0){
                    activateMatchProgress(remainingSecs);
                    relativeLayout.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.color6));
                    if (obstacleCard != null){
                        obstacleCard.setVisibility(View.GONE);
                    }
                    if (editRoundCard != null){
                        editRoundCard.setVisibility(View.GONE);
                    }
                    if (pointsCardView != null){
                        pointsCardView.setVisibility(View.VISIBLE);
                    }
                }else{
                    if (obstacleCard != null){
                        obstacleCard.setVisibility(View.GONE);
                    }
                    if (editRoundCard != null){
                        editRoundCard.setVisibility(View.GONE);
                    }
                    if (pointsCardView != null){
                        pointsCardView.setVisibility(View.VISIBLE);
                    }
                    startButton.setProgress(0);
                    startButton.setCompleteText("Awaiting Approval");
                    startButton.setIdleText("Awaiting Approval");
                    startButton.setProgress(100);
                }
            }else if (roundItem.getIsDone() == 4){
                Log.d("Adapter", "bindData: 1");
                startButton.setCompleteText("Complete");
                startButton.setProgress(100);
                relativeLayout.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.color8));
                if (obstacleCard != null){
                    obstacleCard.setVisibility(View.GONE);
                }
                if (editRoundCard != null){
                    editRoundCard.setVisibility(View.GONE);
                }
                if (pointsCardView != null){
                    pointsCardView.setVisibility(View.VISIBLE);
                }
            }

            group1Name.setText(roundItem.getGroup1());
            group2Name.setText(roundItem.getGroup2());

            if (Config.AdminStatus > 0){
                editRoundCard.setVisibility(View.GONE);
            }

            if (roundItem.getIsDone() != 2) {
                if ((obstacle1Spinner.getSelectedItem().toString()).equals("none")) {
                    leftTop.setVisibility(View.INVISIBLE);
                }
                if ((obstacle2Spinner.getSelectedItem().toString()).equals("none")) {
                    leftBottom.setVisibility(View.INVISIBLE);
                }
                if ((obstacle3Spinner.getSelectedItem().toString()).equals("none")) {
                    rightTop.setVisibility(View.INVISIBLE);
                }
                if ((obstacle4Spinner.getSelectedItem().toString()).equals("none")) {
                    rightBottom.setVisibility(View.INVISIBLE);
                }
            }


            leftTopPoints.setText(roundItem.getObstacleItem1Name());
            leftBottomPoints.setText(roundItem.getObstacleItem2Name());

            rightTopPoints.setText(roundItem.getObstacleItem3Name());
            rightBottomPoints.setText(roundItem.getObstacleItem4Name());



        }else{
            cronometer.setVisibility(View.INVISIBLE);
            startButton.setVisibility(View.INVISIBLE);
        }


        if (isPerma){
            backButton.setVisibility(View.GONE);
        }
    }

    private void initListeners(){
        if (roundItem != null){
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleRequestEditRound();
                    onBackPressed();
                }
            });

            obstacle1Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    if (id > 0) {
                        pointsCardView.setVisibility(View.GONE);
                        if (toastObject != null) {
                            toastObject.cancel();
                        }
                        toastObject = Toast.makeText(getContext(),"You must save changes before you can add points.",Toast.LENGTH_SHORT);
                        toastObject.show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });
            obstacle2Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    if (id > 0) {
                        pointsCardView.setVisibility(View.GONE);
                        if (toastObject != null) {
                            toastObject.cancel();
                        }
                        toastObject = Toast.makeText(getContext(),"You must save changes before you can add points.",Toast.LENGTH_SHORT);
                        toastObject.show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });

            obstacle3Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    if (id > 0) {
                        pointsCardView.setVisibility(View.GONE);
                        if (toastObject != null) {
                            toastObject.cancel();
                        }
                        toastObject = Toast.makeText(getContext(),"You must save changes before you can add points.",Toast.LENGTH_SHORT);
                        toastObject.show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });


            obstacle4Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    if (id > 0) {
                        pointsCardView.setVisibility(View.GONE);
                        if (toastObject != null) {
                            toastObject.cancel();
                        }
                        toastObject = Toast.makeText(getContext(),"You must save changes before you can add points.",Toast.LENGTH_SHORT);
                        toastObject.show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });

            leftTop.setOnClickListener(new ElegantNumberButton.OnClickListener() {
                @Override
                public void onClick(View view) {

                }

                @Override
                public void onClickSign(int i) {
                    if (i>0){
                        handleRequestUpdateGroupPoints(1, 1, 1);

                    }
                    if (i<0){
                        handleRequestUpdateGroupPoints(1, 1, -1);
                    }
                }
            });




            leftBonus.setOnClickListener(new ElegantNumberButton.OnClickListener() {
                @Override
                public void onClick(View view) {

                }

                @Override
                public void onClickSign(int i) {
                    if (i>0){
                        handleRequestUpdateGroupPoints(1, 3, 1);

                    }
                    if (i<0){
                        handleRequestUpdateGroupPoints(1, 3, -1);
                    }
                }
            });



            rightBonus.setOnClickListener(new ElegantNumberButton.OnClickListener() {
                @Override
                public void onClick(View view) {

                }

                @Override
                public void onClickSign(int i) {
                    if (i>0){
                        handleRequestUpdateGroupPoints(2, 3, 1);

                    }
                    if (i<0){
                        handleRequestUpdateGroupPoints(2, 3, -1);
                    }
                }
            });


            leftBottom.setOnClickListener(new ElegantNumberButton.OnClickListener() {
                @Override
                public void onClick(View view) {

                }

                @Override
                public void onClickSign(int i) {
                    if (i>0){
                        handleRequestUpdateGroupPoints(1, 2, 1);

                    }
                    if (i<0){
                        handleRequestUpdateGroupPoints(1, 2, -1);
                    }
                }
            });

            rightTop.setOnClickListener(new ElegantNumberButton.OnClickListener() {
                @Override
                public void onClick(View view) {

                }

                @Override
                public void onClickSign(int i) {
                    if (i>0){
                        handleRequestUpdateGroupPoints(2, 1, 1);

                    }
                    if (i<0){
                        handleRequestUpdateGroupPoints(2, 1, -1);
                    }
                }
            });
            rightBottom.setOnClickListener(new ElegantNumberButton.OnClickListener() {
                @Override
                public void onClick(View view) {

                }

                @Override
                public void onClickSign(int i) {
                    if (i>0){
                        handleRequestUpdateGroupPoints(2, 2, 1);

                    }
                    if (i<0){
                        handleRequestUpdateGroupPoints(2, 2, -1);
                    }
                }
            });

            saveObstacleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleRequestUpdateRoundObstacles(true);
                }
            });

            resetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleRequestResetRound();
                }
            });


        }else{
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleRequestSaveRound();
                    onBackPressed();
                }

            });
        }
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((Config.AdminStatus == 0) && (startButton.getProgress() == 100 || startButton.getProgress() == 1 )) {
                    handleRequestApproveRound();
                } else if((Config.AdminStatus == 0) && startButton.getProgress() <= 0) {
                    handleRequestUpdateRoundObstacles(false);
                    Handler handler = new Handler();
                    final Runnable r = new Runnable() {
                        public void run() {
                            handleRequestStartRound();
                        }
                    };
                    handler.postDelayed(r, 1000);
                }else if((Config.AdminStatus == 0) && startButton.getProgress() > 0) {
                    handleRequestFinishMatchRound();
                }
            }
        });
    }

    private void handleRequestFinishMatchRound() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Do you want the match to move to review?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestReviewRound();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder1.create();
        alert.show();
    }

    private void requestReviewRound() {
        JSONArray params = new JSONArray();
        params.put(roundItem.get_id());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.REVIEW_ROUND,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "review_round");
    }

    private void handleRequestResetRound() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Do you want to reset this round?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestResetRound();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder1.create();
        alert.show();
    }


    public void requestResetRound(){
        JSONArray params = new JSONArray();
        Log.d("RoundFragment", "requestResetRound: id:  "+ roundItem.get_id());
        params.put(roundItem.get_id());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.RESET_ROUND,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                onBackPressed();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "reset_round");
    }




    private void handleRequestApproveRound() {

        JSONArray params = new JSONArray();
        params.put(roundItem.get_id());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.APPROVE_ROUND,params,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                roundItem.setIsDone(4);
                startButton.setCompleteText("Complete");
                startButton.setIdleText("Complete");
                startButton.setProgress(0);
                startButton.setProgress(100);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "approve_round");



    }

    private void handleRequestEditRound() {
        JSONArray params = new JSONArray();
        params.put(roundItem.get_id());
        params.put(group1EditBox.getSelectedItem().toString());
        params.put(group2EditBox.getSelectedItem().toString());
        params.put(changeDurationEditBox.getText());
        //params.put(date.getTime());

        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_ROUND,params,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                if (roundFragDelegate != null){
                    //((RefreshFromString)roundFragDelegate).onStringDataRecieved(response,1);

                    roundFragDelegate = null;
                    onBackPressed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "update_round_obstacles");



    }


    private void activateMatchProgress(final int kkk){
        if (countDownTimer2 != null){
            countDownTimer2.cancel();
        }
        if (countDownTimer1 != null){
            countDownTimer1.cancel();
        }
        countDownTimer2 = new CountDownTimer(kkk, 1000) {

            public void onTick(long millisUntilFinished) {
                if (startButton != null){
                    int prog = 100-(int)(millisUntilFinished*100/kkk);
                    startButton.setProgress(prog);
                }
            }

            public void onFinish() {
                if (startButton != null) {
                    startButton.setProgress(100);

                    if (roundFragDelegate != null){
                        ((RefreshFromString)roundFragDelegate).requestNewRoundData();
                        roundFragDelegate = null;
                    }
                }
            }
        };
        countDownTimer2.start();


        countDownTimer1 = new CountDownTimer(kkk, 1000) {

            public void onTick(long millisUntilFinished) {
                if (cronometer != null) {
                    long second = (millisUntilFinished / 1000) % 60;
                    long minute = (millisUntilFinished / (1000 * 60)) % 60;
                    String time = String.format(Locale.ENGLISH,"%02d:%02d", minute, second);
                    cronometer.setText(time);
                }
            }

            public void onFinish() {
                if (cronometer != null){
                    cronometer.setText("");
                }
            }
        };
        countDownTimer1.start();

    }


    @Override
    public void onPause() {
        if (cronometer != null) {
            cronometer.setText("");
            //cronometer.setTextColor(ContextCompat.getColor(getContext(), R.color.colorRed));
        }
        if (countDownTimer2 != null){
            countDownTimer2.cancel();
        }

        if (countDownTimer1 != null){
            countDownTimer1.cancel();
        }


        super.onPause();
    }


    private void handleRequestUpdateRoundObstacles(final boolean closeFrag) {
        String secondGroup = group2EditBox.getSelectedItem().toString();
        String firstGroup = group1EditBox.getSelectedItem().toString();

        JSONArray params = new JSONArray();
        params.put(roundItem.get_id());
        params.put(obstacle1Spinner.getSelectedItem().toString());
        params.put(obstacle2Spinner.getSelectedItem().toString());
        params.put(obstacle3Spinner.getSelectedItem().toString());
        params.put(obstacle4Spinner.getSelectedItem().toString());

        params.put(firstGroup);
        params.put(secondGroup);

        params.put(changeDurationEditBox.getText());

        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.UPDATE_OBSTACLES,params,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                if (roundFragDelegate != null) {
                    //((RefreshFromString) roundFragDelegate).onStringDataRecieved(response, 1);
                    roundFragDelegate = null;
                    if (closeFrag){
                        onBackPressed();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "update_round_obstacles");

    }


    private void handleRequestStartRound() {

        JSONArray params = new JSONArray();

        String secondGroup = group2EditBox.getSelectedItem().toString();
        if (secondGroup.equals("none")){
            if (toastObject != null) {
                toastObject.cancel();
            }
            toastObject = Toast.makeText(getContext(),"You must choose second group to be able to start the round.",Toast.LENGTH_LONG);
            toastObject.show();
            return;
        }
        params.put(roundItem.get_id());


        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.START_ROUND,params,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if(countDownTimer2 != null) {
                    countDownTimer2.cancel();
                }
                startButton.setProgress(-1);
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "start_round");
    }


    private boolean checkLimits(int group,int obs,int sign){
        boolean wasLastGroup1 = false;
        boolean wasLastObs1 = false;
        boolean wasLastPositive = false;
        if (group == 1)
            wasLastGroup1=true;
        if (obs == 1)
            wasLastObs1=true;
        if (sign == 1)
            wasLastPositive=true;

        Log.d("checkLimits", "checkLimits: started");
        if (wasLastGroup1) {
            if (wasLastObs1) {
                if (wasLastPositive) {
                    if ((current1ValueResult1 +obstacleItem1.getTickPointsAsInt())>obstacleItem1.getMaxTickPointsAsInt()){
                        return false;
                    }
                } else {
                    if ((current1ValueResult1 -obstacleItem1.getTickPointsAsInt()) < 0){
                        return false;
                    }
                }
            } else {
                if (wasLastPositive) {
                    if ((current1ValueResult2 +obstacleItem2.getTickPointsAsInt())>obstacleItem2.getMaxTickPointsAsInt()){
                        return false;
                    }
                } else {
                    if ((current1ValueResult2 -obstacleItem2.getTickPointsAsInt())<0){
                        return false;
                    }
                }
            }
        } else {
            if (wasLastObs1) {
                if (wasLastPositive) {
                    if ((current2ValueResult1 +obstacleItem1.getTickPointsAsInt())>obstacleItem1.getMaxTickPointsAsInt()){
                        return false;
                    }
                } else {
                    if ((current2ValueResult1 -obstacleItem1.getTickPointsAsInt())<0){
                        return false;
                    }
                }
            } else {
                if (wasLastPositive) {
                    if ((current2ValueResult2 +obstacleItem2.getTickPointsAsInt())>obstacleItem2.getMaxTickPointsAsInt()){
                        return false;
                    }
                } else {
                    if ((current2ValueResult2 -obstacleItem2.getTickPointsAsInt())<0){
                        return false;
                    }
                }
            }
        }
        Log.d("checkLimits", "checkLimits: true");
        return true;
    }


    private void handleRequestUpdateGroupPoints(final int group,final int obs,final int sign) {
        // && checkLimits(group,obs,sign)
        if (!updatePointsInAction) {
            updatePointsInAction = true;
            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Updating Points...");
            pDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SERVER_URL + Config.ADD_POINTS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.equals("success")){
                        showTalkbackDurationTooltip ("Points added successfully.");
                    }else if (response.equals("failed")){
                        showTalkbackDurationTooltip ("This obstacle points at Maximum.");
                    }else if (response.equals("stateDone")){
                        showTalkbackDurationTooltip ("The round is already approved.");
                    }else{
                        showTalkbackDurationTooltip ("Round didn't start yet.");
                        if (Config.AdminStatus >0 && Config.AdminStatus<3){
                            if (!response.equals(roundItem.get_id())){
                                FindRoundByIdAndShowPerma findRoundByIdAndShowPerma = new FindRoundByIdAndShowPerma(response);
                                EventBus.getDefault().post(findRoundByIdAndShowPerma);
                            }
                        }
                    }
                    updatePointsInAction = false;
                    pDialog.cancel();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    updatePointsInAction = false;
                    VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                    Context con = getContext();
                    if (con != null){
                        if (toastObject != null) {
                            toastObject.cancel();
                        }
                        toastObject = Toast.makeText(con,"Adding Points Failed!",Toast.LENGTH_LONG);
                        toastObject.show();
                    }
                    con = null;
                    pDialog.cancel();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    if (group == 1) {
                        wasLastGroup1 = true;
                        params.put("groupName", roundItem.getGroup1());
                        if (obs == 1) {
                            wasLastObs1 = true;
                            params.put("pointChangeId", "1");
                            if (sign == 1) {
                                wasLastPositive = true;
                                int afterAdd = roundItem.getObs1Points()+obstacleItem1.getTickPointsAsInt();
                                if (obstacleItem1.getMaxTickPointsAsInt() != 0 && afterAdd/obstacleItem1.getMaxTickPointsAsInt() == 1 && afterAdd%obstacleItem1.getMaxTickPointsAsInt()>0){
                                    Integer t = obstacleItem1.getTickPointsAsInt()-afterAdd%obstacleItem1.getMaxTickPointsAsInt();
                                    params.put("points", t.toString());
                                }else
                                    params.put("points", obstacleItem1.getTickPoints());
                            } else {
                                wasLastPositive = false;
                                if (roundItem.getObs1Points() == obstacleItem1.getMaxTickPointsAsInt()){
                                    Integer t = roundItem.getObs1Points()+obstacleItem1.getTickPointsAsInt();
                                    if (obstacleItem1.getTickPointsAsInt() != 0 && t%obstacleItem1.getTickPointsAsInt()>0){
                                        Integer f = t%obstacleItem1.getTickPointsAsInt();
                                        params.put("points", "-" + f.toString());
                                    }else
                                        params.put("points", "-" + obstacleItem1.getTickPoints());
                                }else{
                                    params.put("points", "-" + obstacleItem1.getTickPoints());
                                }
                            }
                        } else if (obs == 3) {
                            params.put("pointChangeId", "5");
                            if (sign == 1) {
                                wasLastPositive = true;
                                params.put("points", "2");
                            } else {
                                wasLastPositive = false;
                                params.put("points", "-2");
                            }
                        }
                        else {
                            wasLastObs1 = false;
                            params.put("pointChangeId", "2");
                            if (sign == 1) {
                                wasLastPositive = true;
                                int afterAdd = roundItem.getObs2Points()+obstacleItem2.getTickPointsAsInt();
                                if (obstacleItem2.getMaxTickPointsAsInt() != 0 && afterAdd/obstacleItem2.getMaxTickPointsAsInt() == 1 && afterAdd%obstacleItem2.getMaxTickPointsAsInt()>0){
                                    Integer t = obstacleItem2.getTickPointsAsInt()-afterAdd%obstacleItem2.getMaxTickPointsAsInt();
                                    params.put("points", t.toString());
                                }else
                                    params.put("points", obstacleItem2.getTickPoints());
                            } else {
                                wasLastPositive = false;
                                if (roundItem.getObs2Points() == obstacleItem2.getMaxTickPointsAsInt()){
                                    Integer t = roundItem.getObs2Points()+obstacleItem2.getTickPointsAsInt();
                                    if (obstacleItem2.getTickPointsAsInt() != 0 && t%obstacleItem2.getTickPointsAsInt()>0){
                                        Integer f = t%obstacleItem2.getTickPointsAsInt();
                                        params.put("points", "-" + f.toString());
                                    }else
                                        params.put("points", "-" + obstacleItem2.getTickPoints());
                                }else{
                                    params.put("points", "-" + obstacleItem2.getTickPoints());
                                }
                            }
                        }
                    } else {
                        wasLastGroup1 = false;
                        params.put("groupName", roundItem.getGroup2());
                        if (obs == 1) {
                            wasLastObs1 = true;
                            params.put("pointChangeId", "3");
                            if (sign == 1) {
                                wasLastPositive = true;
                                int afterAdd = roundItem.getObs3Points()+obstacleItem3.getTickPointsAsInt();
                                if (obstacleItem3.getMaxTickPointsAsInt() != 0 && afterAdd/obstacleItem3.getMaxTickPointsAsInt() == 1 && afterAdd%obstacleItem3.getMaxTickPointsAsInt()>0){
                                    Integer t = obstacleItem3.getTickPointsAsInt()-afterAdd%obstacleItem3.getMaxTickPointsAsInt();
                                    params.put("points", t.toString());
                                }else
                                    params.put("points", obstacleItem3.getTickPoints());
                            } else {
                                wasLastPositive = false;
                                if (roundItem.getObs3Points() == obstacleItem3.getMaxTickPointsAsInt()){
                                    Integer t = roundItem.getObs3Points()+obstacleItem3.getTickPointsAsInt();
                                    if (obstacleItem3.getTickPointsAsInt() != 0 && t%obstacleItem3.getTickPointsAsInt()>0){
                                        Integer f = t%obstacleItem3.getTickPointsAsInt();
                                        params.put("points", "-" + f.toString());
                                    }else
                                        params.put("points", "-" + obstacleItem3.getTickPoints());
                                }else{
                                    params.put("points", "-" + obstacleItem3.getTickPoints());
                                }
                            }
                        }  else if (obs == 3) {
                            params.put("pointChangeId", "6");
                            if (sign == 1) {
                                wasLastPositive = true;
                                params.put("points", "2");
                            } else {
                                wasLastPositive = false;
                                params.put("points", "-2");
                            }
                        }
                        else {
                            wasLastObs1 = false;
                            params.put("pointChangeId", "4");
                            if (sign == 1) {
                                wasLastPositive = true;
                                int afterAdd = roundItem.getObs4Points()+obstacleItem4.getTickPointsAsInt();
                                if (obstacleItem4.getMaxTickPointsAsInt() != 0 && afterAdd/obstacleItem4.getMaxTickPointsAsInt() == 1 && afterAdd%obstacleItem4.getMaxTickPointsAsInt()>0){
                                    Integer t = obstacleItem4.getTickPointsAsInt()-afterAdd%obstacleItem4.getMaxTickPointsAsInt();
                                    params.put("points", t.toString());
                                }else
                                    params.put("points", obstacleItem4.getTickPoints());
                            } else {
                                wasLastPositive = false;
                                if (roundItem.getObs4Points() == obstacleItem4.getMaxTickPointsAsInt()){
                                    Integer t = roundItem.getObs4Points()+obstacleItem4.getTickPointsAsInt();
                                    if (obstacleItem4.getTickPointsAsInt() != 0 && t%obstacleItem4.getTickPointsAsInt()>0){
                                        Integer f = t%obstacleItem4.getTickPointsAsInt();
                                        params.put("points", "-" + f.toString());
                                    }else
                                        params.put("points", "-" + obstacleItem4.getTickPoints());
                                }else{
                                    params.put("points", "-" + obstacleItem4.getTickPoints());
                                }
                            }
                        }
                    }
                    params.put("roundId", roundItem.get_id());
                    return params;
                }
            };
            MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "add_points");
        }
    }

    private void handleRequestDeleteRound() {
        roundFragDelegate.onDeleteRound(roundItem.get_id());
    }


    private void handleRequestSaveRound() {

        String secondGroup = group2EditBox.getSelectedItem().toString();
        if (secondGroup.equals("none")){
            if (toastObject != null) {
                toastObject.cancel();
            }
            toastObject = Toast.makeText(getContext(),"You must choose second group to be able save.",Toast.LENGTH_LONG);
            toastObject.show();
            return;
        }
        JSONArray params = new JSONArray();
        params.put(group1EditBox.getSelectedItem().toString());
        params.put(secondGroup);
        params.put(changeDurationEditBox.getText());
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.ADD_NEW_ROUND,params,new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                if (roundFragDelegate != null) {
                    //((RefreshFromString) roundFragDelegate).onStringDataRecieved(response, 1);
                    roundFragDelegate = null;
                    onBackPressed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "add_new_round");

    }

    void onBackPressed(){
        if (!isPerma){
            getFragmentManager().popBackStack();
        }
    }


}
