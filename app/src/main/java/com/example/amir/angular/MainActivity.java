package com.example.amir.angular;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import im.delight.android.ddp.Meteor;
import im.delight.android.ddp.MeteorCallback;
import im.delight.android.ddp.ResultListener;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements FragmentCalls,RefreshFromString,RoundFragDelegate, MeteorCallback,SettingsDelegate {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    public static String TAG = "MainActivity";
    private FloatingActionButton fab;
    private int currentTabPos;
    private AnimationDrawable anim;
    private Meteor mMeteor;
    private JSONObject lastData;
    private String lastDataString;
    private String lastAction = "";
    private Menu menu;
    private ArrayList<RoundOrder> roundorderList;
    private String dataBackedOnServer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        SharedPreferences sharedPreferences = getSharedPreferences("userdetails", MODE_PRIVATE);
        if (sharedPreferences.getString("saved_ip_address","null").equals("null")) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("saved_ip_address",Config.SERVER_URL);
            edit.apply();
            edit.commit();
        }else{
            Config.SERVER_URL = sharedPreferences.getString("saved_ip_address","null");
        }
        if (sharedPreferences.getInt("admintype",-1) == -1) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putInt("admintype",Config.AdminStatus);
            edit.apply();
        }else{
            Config.AdminStatus = sharedPreferences.getInt("admintype",0);
        }




        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);

        anim = (AnimationDrawable) mViewPager.getBackground();
        anim.setEnterFadeDuration(6000);
        anim.setExitFadeDuration(2000);


        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(5);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentTabPos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentTabPos == 1){
                    CreateNewRound t = new CreateNewRound(MainActivity.this);
                    t.show();
                }else if (currentTabPos == 0){
                    CreateNewGroup t = new CreateNewGroup(MainActivity.this);
                    t.show();
                }else if (currentTabPos == 2){
                    CreateNewObstacle t = new CreateNewObstacle(MainActivity.this);
                    t.show();
                }else{
                    CreateNewStage t = new CreateNewStage(MainActivity.this);
                    t.show();
                }

            }
        });
        if(Config.AdminStatus > 0){
            fab.setVisibility(View.GONE);
        }
        setStatusBarColor(getResources().getColor(R.color.colorPrimary));

        mMeteor = new Meteor(this, "ws://"+getSocketServer()+"/websocket");

        // register the callback that will handle events and receive messages
        mMeteor.addCallback(this);

        // establish the connection
        //mMeteor.connect();
        //mMeteor.addCallback(callback);
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
        Fabric.with(this, new Crashlytics());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void editRoundOrder(EditRoundOrder event){
        CreateNewStage t = new CreateNewStage(MainActivity.this);
        t.setOrderForEdit(event.getRoundOrder());
        t.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateBackup(SaveBackupOnDevice event){
        SharedPreferences sharedPreferences = getSharedPreferences("userdetails", MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("back_up_data_on_device",dataBackedOnServer);
        edit.apply();
    }





    @Subscribe(threadMode = ThreadMode.MAIN)
    public void openPermaFrag(PermaRoundFragmentDelegate event){
        if (Config.AdminStatus>0 && Config.AdminStatus<3 ){
            Log.d(TAG, "openPermaFrag: reached buss");
            openPermaRoundFragmentForJudge(event.getRoundItem());
        }
    }




    private String getSocketServer(){
        if (Config.SERVER_URL.length()>5) {
            if (Config.SERVER_URL.substring(0, 5).equals("https")) {
                Log.d(TAG, "getSocketServer: https");
                return Config.SERVER_URL.substring(8, Config.SERVER_URL.length());
            } else if (Config.SERVER_URL.substring(0, 4).equals("http")) {
                Log.d(TAG, "getSocketServer: http");
                return Config.SERVER_URL.substring(7, Config.SERVER_URL.length());
            }
        }
        Log.d(TAG, "getSocketServer: null");
        return "null";
    }


    @Override
    public void onBackPressed() {
        FragmentManager fmgr = getSupportFragmentManager();
        Fragment f = fmgr.findFragmentByTag("permaFragment");
        if (f == null){
            Log.d(TAG, "onBackPressed: no perma frag on");
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (anim != null && !anim.isRunning())
            anim.start();

        if (mMeteor != null && menu != null && !mMeteor.isConnected())
            mMeteor.connect();
        FragmentManager fmgr = getSupportFragmentManager();
        if (fmgr != null){
            fmgr.executePendingTransactions();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (anim != null && anim.isRunning())
            anim.stop();
    }

    public void setStatusBarColor(int color){
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor( color );
        }
    }

    private void requestGetGroups() {
        Log.d(TAG, "requestGetGroups: ");
        String tag_json_obj = "getGroupsRequest";
        String url = Config.SERVER_URL + Config.GET_GROUPS;

        if (mMeteor != null && !mMeteor.isConnected())
            mMeteor.connect();
        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //Snackbar.make(fab, "Response Arrived", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        pDialog.hide();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(0);
                        frag.setData(response,0);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    private void requestGetObstacles(){
        Log.d(TAG, "requestGetObstacles: ");
        String tag_json_obj = "getObstaclesRequest";
        String url = Config.SERVER_URL + Config.GET_OBSTACLES;
        if (mMeteor != null && !mMeteor.isConnected())
            mMeteor.connect();
        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.hide();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(2);
                        frag.setData(response,2);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }



    private void requestGetRounds() {
        Log.d(TAG, "requestGetRounds: ");
        String tag_json_obj = "getRoundsRequest";
        String url = Config.SERVER_URL + Config.GET_ROUNDS;
        if (mMeteor != null && !mMeteor.isConnected())
            mMeteor.connect();
        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(url,null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.hide();

                        try {
                            JSONArray data = response.getJSONArray("data");
                            JSONObject roundOrderJson = response.getJSONObject("roundOrder");

                            Integer stage = roundOrderJson.getInt("stage");

                            FragmentManager fragmentManager = getSupportFragmentManager();
                            PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(1);
                            frag.setData(data,1);

                            for (int i=0;i<roundorderList.size();i++){
                                RoundOrder roundorder = roundorderList.get(i);
                                if (roundorder.getStage() == stage){
                                    roundorder.setList(roundOrderJson.getJSONArray("order"));
                                    roundorder.setName(roundOrderJson.getString("name"));
                                    roundorder.setStageType(roundOrderJson.getInt("type"));
                                    roundorder.setRoundTime(roundOrderJson.getInt("roundDuration"));
                                    roundorder.setReviewTime(roundOrderJson.getInt("reviewDuration"));
                                    EventBus.getDefault().post(roundorder);
                                    break;
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }


    private void requestSetStage(int stage) {
        Log.d(TAG, "requestGetPlayoffs: ");
        String tag_json_obj = "requestGetPlayoffs";
        String url = Config.SERVER_URL + Config.GET_PLAYOFFS;
        if (mMeteor != null && !mMeteor.isConnected())
            mMeteor.connect();
        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        JSONArray params = new JSONArray();
        params.put(stage);
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.POST,url,params,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.hide();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(1);
                        frag.setData(response,1);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }


    private void requestShowWinners() {
        Log.d(TAG, "requestShowWinners: ");
        String tag_json_obj = "requestShowWinners";
        String url = Config.SERVER_URL + Config.SHOW_WINNERS;
        if (mMeteor != null && !mMeteor.isConnected())
            mMeteor.connect();
        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.hide();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(1);
                        frag.setData(response,1);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    private void requestDeleteGroup(String _id) {
        Log.d(TAG, "requestDeleteGroup: ");
        String tag_json_obj = "DeleteGroupRequest";
        String url = Config.SERVER_URL + Config.DELETE_GROUPS + "/" + _id;

        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.hide();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(0);
                        frag.setData(response,0);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    private void requestDeleteStage(String _id) {
        Log.d(TAG, "requestDeleteStage: ");
        String tag_json_obj = "requestDeleteStageRequest";
        String url = Config.SERVER_URL + Config.DELETE_STAGE + "/" + _id;;

        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.hide();
                        //FragmentManager fragmentManager = getSupportFragmentManager();
                        //PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(2);
                        //frag.setData(response,2);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void requestDeleteObstacle(String _id) {
        Log.d(TAG, "requestDeleteObstacle: ");
        String tag_json_obj = "DeleteObstacleRequest";
        String url = Config.SERVER_URL + Config.DELETE_OBSTACLE + "/" + _id;;

        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.hide();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(2);
                        frag.setData(response,2);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void requestDeleteRound(String _id) {
        Log.d(TAG, "requestDeleteGroup: ");
        String tag_json_obj = "DeleteGroupRequest";
        String url = Config.SERVER_URL + Config.DELETE_ROUNDS + "/" + _id;;

        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.hide();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(1);
                        frag.setData(response,1);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;
        if (mMeteor != null && !mMeteor.isConnected())
            mMeteor.connect();
        if (Config.AdminStatus==0) {
            menu.getItem(1).setVisible(true);
            menu.getItem(2).setVisible(true);
        }
        return true;
    }

    /*
    public void onFabClick(View v){
        if (mViewPager.getCurrentItem() == 1){
            FragmentManager fmgr = getSupportFragmentManager();
            fmgr.beginTransaction().setCustomAnimations(R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left,R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left).addToBackStack("containterFragment").replace(R.id.fragment_container,new RoundFragment()).commitAllowingStateLoss();
        }
    }
    */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initiateRefresh(int id) {
        Log.d(TAG, "initiateRefresh:  id: "+id);

        if (id == 0){
            requestGetGroups();
        }
        else if (id == 1){
            requestGetRounds();
        }else if (id == 2){
            requestGetObstacles();
        }
    }

    @Override
    public void onCardViewClick(int adapterId, RecyclerItem recyclerItem) {
        if(adapterId == 1){
            //RoundItem roundItem = (RoundItem)recyclerItem;
            FragmentManager fmgr = getSupportFragmentManager();
            RoundFragment roundFragment = new RoundFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("parcel_item",(Parcelable)recyclerItem);
            bundle.putBoolean("isPerma",false);
            roundFragment.setArguments(bundle);
            fmgr.beginTransaction().setCustomAnimations(R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left,R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left).addToBackStack("containterFragment").replace(R.id.fragment_container,roundFragment).commitAllowingStateLoss();
        }else if(adapterId == 3){
            RoundOrder roundOrder = (RoundOrder) recyclerItem;
            requestSetStage(roundOrder.getStage());
        }
    }


    public void openPermaRoundFragmentForJudge(RecyclerItem recyclerItem){
        FragmentManager fmgr = getSupportFragmentManager();
        RoundFragment roundFragment = new RoundFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("parcel_item",(Parcelable)recyclerItem);
        bundle.putBoolean("isPerma",true);
        roundFragment.setArguments(bundle);
        RoundFragment f = (RoundFragment)fmgr.findFragmentByTag("permaFragment");
        if (f != null){
            Log.d(TAG, "openPermaRoundFragmentForJudge: restarted? "+f.isRoundRestarted());
            if (!f.getRoundItemId().equals(recyclerItem.get_id()) || f.isRoundRestarted()){
                fmgr.beginTransaction().remove(f).commitAllowingStateLoss();
                fmgr.beginTransaction().setCustomAnimations(R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left,R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left).replace(R.id.fragment_container,roundFragment,"permaFragment").commitAllowingStateLoss();
            }
        }else
            fmgr.beginTransaction().setCustomAnimations(R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left,R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left).replace(R.id.fragment_container,roundFragment,"permaFragment").commitAllowingStateLoss();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    };

    public void closePermaRoundFragmentForJudge(){
        FragmentManager fmgr = getSupportFragmentManager();
        Fragment f = fmgr.findFragmentByTag("permaFragment");
        if (f != null){
            fmgr.beginTransaction().remove(f).commit();
        }
    }




    @Override
    public void onCardViewLongClick(int adapterId, @Nullable final String itemIdentifier) {
        if (adapterId == 0){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Do you want to delete this group?");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            requestDeleteGroup(itemIdentifier);
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }else if (adapterId == 1){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Do you want to delete this round?");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            requestDeleteRound(itemIdentifier);
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(1);
                            frag.notifyAdapterToDataSetChange();
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }else if (adapterId == 2){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Do you want to delete this obstacle?");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            requestDeleteObstacle(itemIdentifier);
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }else if (adapterId == 3){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Do you want to delete this stage?");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            requestDeleteStage(itemIdentifier);
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            PlaceholderFragment frag =(PlaceholderFragment)fragmentManager.getFragments().get(3);
                            frag.notifyAdapterToDataSetChange();
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }

    }

    @Override
    public void onStringDataRecieved(JSONArray data,int type) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            PlaceholderFragment frag = (PlaceholderFragment) fragmentManager.getFragments().get(type);
            frag.setData(data, type);
    }
    @Override
    public void requestNewRoundData(){
        requestGetRounds();
    }

    @Override
    public void onDeleteRound(String id) {
        requestDeleteRound(id);
    }

    public void openSettingsFrag(MenuItem item) {
        FragmentManager fmgr = getSupportFragmentManager();
        SettingsFragment settingsFragment = new SettingsFragment();
        fmgr.beginTransaction().setCustomAnimations(R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left,R.anim.slide_from_right_to_left,R.anim.slide_to_exit_left).addToBackStack("settingsFragment").replace(R.id.fragment_container,settingsFragment).commitAllowingStateLoss();
    }

    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    public void SwitchToGameStage1(MenuItem item) {
        requestSetStage(1);
    }

    public void SwitchToGameStage2(MenuItem item) {
        requestSetStage(2);
    }

    public void SwitchToPlayoffs(MenuItem item) {
        requestSetStage(3);
    }

    public void calculatePlayoffs(MenuItem item) {
        Log.d(TAG, "requestCalculatePlayoffs: ");
        String tag_json_obj = "requestCalculatePlayoffs";
        String url = Config.SERVER_URL + Config.CALCULATE_PLAYOFFS;
        if (mMeteor != null && !mMeteor.isConnected())
            mMeteor.connect();
        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        JSONArray params = new JSONArray();
        params.put(Config.GameStatus);
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.POST,url,params,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        pDialog.hide();
                        try {
                            if (response.getString(0).equals("This is not playoff stage!")){
                                Toast.makeText(getBaseContext(),"You must pick Playoff stage first!",Toast.LENGTH_SHORT).show();
                            }else if (response.getString(0).equals("need atleast 4 groups")){
                                Toast.makeText(getBaseContext(),"You must have atleast 4 groups to calculate playoff!",Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error);
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // hide the progress dialog
                pDialog.hide();
            }
        });
        MonaAdminApp.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

    }

    public void showWinners(MenuItem item) {
        requestShowWinners();
    }

    @Override
    public void onConnect(boolean signedInAutomatically) {
        Log.d(TAG, "onConnect: ");
        mMeteor.call("getServerTime", new ResultListener() {
            @Override
            public void onSuccess(String result) {
                Config.serverDifTime = System.currentTimeMillis()-Long.parseLong(result);
            }

            @Override
            public void onError(String error, String reason, String details) {

            }
        });
        EventBus.getDefault().post(new DeleteAllData());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void switchRefreshTables(SwitchRefreshTables event) {
        mMeteor.call("switchRefreshTables", new ResultListener() {
            @Override
            public void onSuccess(String result) {
                if (result.equals("true")){
                    Toast.makeText(getBaseContext(),"Tables will automatically switch.",Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(getBaseContext(),"Tables will remain still.",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String error, String reason, String details) {

            }
        });
    }


    @Override
    public void onDisconnect() {
        Log.d(TAG, "onDisconnect: ");
    }

    @Override
    public void onException(Exception e) {
        Log.d(TAG, "onException: "+e);
    }

    @Override
    public void onDataAdded(String collectionName, String documentID, String newValuesJson) {
        Log.d(TAG, "onDataAdded: collectionName:"+collectionName+": "+newValuesJson);
        if (collectionName.equals("rounds")){
            try {
                JSONObject jsonObject = new JSONObject(newValuesJson);
                RoundItem recyclerItem = new RoundItem();
                recyclerItem.set_id(documentID);
                recyclerItem.setGroup1(jsonObject.getString("leftSide"));
                recyclerItem.setGroup2(jsonObject.getString("rightSide"));
                recyclerItem.setIsDone(jsonObject.getInt("isDone"));
                recyclerItem.setRoundStage(jsonObject.getInt("stage"));
                if (recyclerItem.getIsDone() >= 3){
                    recyclerItem.setMatchEnd(-1);
                }else{
                    try{
                        recyclerItem.setMatchEnd(jsonObject.getLong("executeTime"));
                    }catch(Throwable e){
                        recyclerItem.setMatchEnd(-1);
                    }
                }

                try{
                    recyclerItem.setRoundTime(jsonObject.getInt("timeLeft"));
                }catch(Throwable e){
                    recyclerItem.setRoundTime(1);
                }

                try{
                    recyclerItem.setObstacleItem1Name(jsonObject.getString("obstacle1"));
                }catch(Throwable e){
                    recyclerItem.setObstacleItem1Name("none");
                }
                try{
                    recyclerItem.setObstacleItem2Name(jsonObject.getString("obstacle2"));
                }catch(Throwable e){
                    recyclerItem.setObstacleItem2Name("none");
                }
                try{
                    recyclerItem.setObstacleItem3Name(jsonObject.getString("obstacle3"));
                }catch(Throwable e){
                    recyclerItem.setObstacleItem3Name("none");
                }
                try{
                    recyclerItem.setObstacleItem4Name(jsonObject.getString("obstacle4"));
                }catch(Throwable e){
                    recyclerItem.setObstacleItem4Name("none");
                }

                try{
                    recyclerItem.setObs1Points(jsonObject.getInt("obs1Points"));
                }catch(JSONException e){
                    recyclerItem.setObs1Points(0);
                }
                try{
                    recyclerItem.setObs2Points(jsonObject.getInt("obs2Points"));
                }catch(JSONException e){
                    recyclerItem.setObs2Points(0);

                }
                try{
                    recyclerItem.setObs3Points(jsonObject.getInt("obs3Points"));
                }catch(JSONException e){
                    recyclerItem.setObs3Points(0);
                }
                try{
                    recyclerItem.setObs4Points(jsonObject.getInt("obs4Points"));
                }catch(JSONException e){
                    recyclerItem.setObs4Points(0);
                }


                try{
                    recyclerItem.setLeftBonusPoints(jsonObject.getInt("leftBonus"));
                }catch(JSONException e){
                    recyclerItem.setLeftBonusPoints(0);
                }

                try{
                    recyclerItem.setRightBonusPoints(jsonObject.getInt("rightBonus"));
                }catch(JSONException e){
                    recyclerItem.setRightBonusPoints(0);
                }

                EventBus.getDefault().post(recyclerItem);

                if (Config.AdminStatus > 0 && Config.AdminStatus < 3 && recyclerItem.getIsDone() > 1 && recyclerItem.getIsDone() < 4){
                    openPermaRoundFragmentForJudge(recyclerItem);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else if (collectionName.equals("groups")){
            try {
                JSONObject jsonObject = new JSONObject(newValuesJson);
                GroupItem recyclerItem = new GroupItem();
                recyclerItem.set_id(documentID);
                recyclerItem.setGroupName(jsonObject.getString("groupName"));
                recyclerItem.setGroupPoints(jsonObject.getInt("points"));
                try{
                    recyclerItem.setDescription(jsonObject.getString("description"));
                }catch(Throwable e){
                    recyclerItem.setDescription("");
                }
                EventBus.getDefault().post(recyclerItem);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if (collectionName.equals("obstacles")){
            try {
                JSONObject jsonObject = new JSONObject(newValuesJson);
                ObstacleItem recyclerItem = new ObstacleItem();
                recyclerItem.set_id(documentID);
                recyclerItem.setName(jsonObject.getString("name"));
                recyclerItem.setDescription(jsonObject.getString("description"));
                recyclerItem.setOwner(jsonObject.getString("owner"));
                recyclerItem.setMaxPoints(jsonObject.getString("maxPoints"));
                recyclerItem.setTickPoints(jsonObject.getString("tickPoints"));
                EventBus.getDefault().post(recyclerItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else if (collectionName.equals("game_state")){
            try {
                JSONObject jsonObject = new JSONObject(newValuesJson);
                int t = jsonObject.getInt("gameStage");
                if (Config.GameStatus != t){
                    Config.GameStatus = t;
                    Log.d(TAG, "onDataAdded: gameStageDetected "+t);
                    if (roundorderList == null) return;
                    for (int i=0;i<roundorderList.size();i++){
                        RoundOrder roundorder = roundorderList.get(i);
                        if (roundorder.getStage() == Config.GameStatus){
                            Config.Stage = roundorder;
                            EventBus.getDefault().post(roundorder);
                            EventBus.getDefault().post(new SelectedStageEvent(roundorder.get_id()));
                            break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if (collectionName.equals("roundsorder")) {
            if (roundorderList == null) {
                roundorderList = new ArrayList<>();
            }
            try {
                JSONObject jsonObject = new JSONObject(newValuesJson);
                int stage = jsonObject.getInt("stage");
                String name = jsonObject.getString("name");
                JSONArray order = jsonObject.getJSONArray("order");
                boolean foundOld = false;
                for (int i = 0; i < roundorderList.size(); i++) {
                    RoundOrder roundorder = roundorderList.get(i);
                    if (roundorder.getStage() == stage) {
                        roundorder.setList(order);
                        roundorder.setName(name);
                        roundorder.setStageType(jsonObject.getInt("type"));
                        roundorder.setRoundTime(jsonObject.getInt("roundDuration"));
                        roundorder.setReviewTime(jsonObject.getInt("reviewDuration"));
                        if (Config.GameStatus == roundorder.getStage()) {
                            Config.Stage = roundorder;
                            EventBus.getDefault().post(new SelectedStageEvent(roundorder.get_id()));
                        }
                        EventBus.getDefault().post(roundorder);
                        foundOld = true;
                        break;
                    }
                }
                if (!foundOld) {
                    RoundOrder newRoundOrder = new RoundOrder(order, documentID, stage);
                    newRoundOrder.setName(name);
                    newRoundOrder.setStageType(jsonObject.getInt("type"));
                    newRoundOrder.setRoundTime(jsonObject.getInt("roundDuration"));
                    newRoundOrder.setReviewTime(jsonObject.getInt("reviewDuration"));
                    roundorderList.add(newRoundOrder);
                    if (Config.GameStatus == newRoundOrder.getStage()) {
                        Config.Stage = newRoundOrder;
                        EventBus.getDefault().post(new SelectedStageEvent(newRoundOrder.get_id()));
                    }
                    Log.d(TAG, "onDataAdded: new round order added");
                    EventBus.getDefault().post(newRoundOrder);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else if (collectionName.equals("backups")) {
            dataBackedOnServer = newValuesJson;


        }else if (collectionName.equals("androidevents")) {
            String s;
            try {
                JSONObject jsonObject = new JSONObject(newValuesJson);
                s = jsonObject.getString("action");
                lastAction = s;
            } catch (JSONException e) {
            }
        }
    }

    @Override
    public void onDataChanged(String collectionName, String documentID, String updatedValuesJson, String removedValuesJson) {
        //Log.d(TAG, "onDataChanged: "+collectionName);
        if (collectionName.equals("androidevents")){
            String action;
            String s;
            try {
                JSONObject jsonObject = new JSONObject(updatedValuesJson);
                s = jsonObject.getString("action");
                lastAction = s;
                action = lastAction;
            }catch(JSONException e){
                action = lastAction;
            }
            Log.d(TAG, "onDataChanged: action: "+action);
            try{
                JSONObject jsonObject = new JSONObject(updatedValuesJson);
                Log.d(TAG, "onDataChanged: updatedValuesJson: "+updatedValuesJson);
                    if (action.equals("statusDone")){
                        RoundStatusEvent t = new RoundStatusEvent();
                        String k;
                        try {
                            k = jsonObject.getString("data");
                            lastDataString = k;
                        }catch(JSONException e){
                            k = lastDataString;
                        }
                        t.set_id(k);
                        t.setStatus(4);
                        EventBus.getDefault().post(t);
                    }else if (action.equals("statusReview")){
                        RoundStatusEvent t = new RoundStatusEvent();
                        String k;
                        try {
                            k = jsonObject.getString("data");
                            lastDataString = k;
                        }catch(JSONException e){
                            k = lastDataString;
                        }
                        t.set_id(k);
                        t.setStatus(3);
                        EventBus.getDefault().post(t);
                    }else if (action.equals("statusReady")){
                        RoundStatusEvent t = new RoundStatusEvent();
                        String k;
                        try {
                            k = jsonObject.getString("data");
                            lastDataString = k;
                        }catch(JSONException e){
                            k = lastDataString;
                        }
                        t.set_id(k);
                        t.setStatus(1);
                        Log.d(TAG, "onDataChanged: reset? k: "+k);
                        EventBus.getDefault().post(t);
                    }else if (action.equals("startRound")){
                        try {
                            JSONObject jsonObjectData = new JSONObject(jsonObject.getString("data"));
                            RoundStatusEvent t = new RoundStatusEvent();
                            t.set_id(jsonObjectData.getString("_id"));
                            t.setCounterEnds(jsonObjectData.getInt("timeLeft"));
                            t.setStatus(2);
                            t.setObsType(jsonObjectData.getInt("obsType"));
                            t.setObs1tick(jsonObjectData.getInt("obs1tick"));
                            t.setObs2tick(jsonObjectData.getInt("obs2tick"));
                            t.setObs3tick(jsonObjectData.getInt("obs3tick"));
                            t.setObs4tick(jsonObjectData.getInt("obs4tick"));
                            EventBus.getDefault().post(t);
                        }catch(JSONException e){

                        }
                    }else if (action.equals("pointsUpdate")){
                        /*
                        Log.d(TAG, "pointsUpdate: added points");
                        JSONObject jsonObjectData = new JSONObject(jsonObject.getString("data"));
                        PointsStatusEvent t = new PointsStatusEvent();
                        t.set_id(jsonObjectData.getString("_id"));
                        t.setLeftobs1(jsonObjectData.getInt("leftobs1"));
                        t.setLeftobs2(jsonObjectData.getInt("leftobs2"));
                        t.setRightobs1(jsonObjectData.getInt("rightobs1"));
                        t.setRightobs2(jsonObjectData.getInt("rightobs2"));

                        t.setLeftBonus(jsonObjectData.getInt("leftBonus"));
                        t.setRightBonus(jsonObjectData.getInt("rightBonus"));
                        EventBus.getDefault().post(t);
                        */
                    }else if (action.equals("resetRounds")){
                        EventBus.getDefault().post(new DeleteAllRounds());
                    }
            }catch(JSONException e){
                e.printStackTrace();
            }
        }else if (collectionName.equals("rounds")){
            try {
                Log.d(TAG, "round change: "+updatedValuesJson);
                JSONObject jsonObject = new JSONObject(updatedValuesJson);
                UpdateRoundDelegate updateRoundDelegate  = new UpdateRoundDelegate(documentID,jsonObject);
                EventBus.getDefault().post(updateRoundDelegate);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else if (collectionName.equals("groups")){
            try {
                Log.d(TAG, "groups change: "+updatedValuesJson);
                JSONObject jsonObject = new JSONObject(updatedValuesJson);
                UpdateGroupDelegate updateGroupDelegate  = new UpdateGroupDelegate(documentID,jsonObject);
                EventBus.getDefault().post(updateGroupDelegate);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if (collectionName.equals("game_state")){
            try {
                JSONObject jsonObject = new JSONObject(updatedValuesJson);
                int t = jsonObject.getInt("gameStage");
                if (Config.GameStatus != t){
                    Config.GameStatus = t;
                    updateStageType();
                    Log.d(TAG, "onDatachanged: gameStageDetected "+t);

                    for (int i=0;i<roundorderList.size();i++){
                        RoundOrder roundorder = roundorderList.get(i);
                        if (roundorder.getStage() == Config.GameStatus){
                            EventBus.getDefault().post(roundorder);
                            break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if (collectionName.equals("roundsorder")){

            if (roundorderList == null){
                roundorderList = new ArrayList<>();
            }
            try {
                JSONObject jsonObject = new JSONObject(updatedValuesJson);
                for (int i=0;i<roundorderList.size();i++){
                    final RoundOrder roundorder = roundorderList.get(i);
                    if (roundorder.get_id().equals(documentID)){
                        try {
                            JSONArray order = jsonObject.getJSONArray("order");
                            roundorder.setList(order);
                        }catch(JSONException e){}

                        try {
                            roundorder.setStageType(jsonObject.getInt("type"));
                        }catch(JSONException e){}

                        try {
                            roundorder.setRoundTime(jsonObject.getInt("roundDuration"));
                        }catch(JSONException e){}
                        try {
                            roundorder.setReviewTime(jsonObject.getInt("reviewDuration"));
                        }catch(JSONException e){}

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (Config.GameStatus == roundorder.getStage()) {
                                    Config.Stage = roundorder;
                                    EventBus.getDefault().post(new SelectedStageEvent(roundorder.get_id()));
                                }
                                EventBus.getDefault().post(roundorder);
                            }
                        }, 200);


                        break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, "onDataChanged: "+updatedValuesJson);
    }

    private void updateStageType() {
        for(RoundOrder roundOrder:roundorderList){
            if (roundOrder.getStage() == Config.GameStatus){
                Config.Stage = roundOrder;
                EventBus.getDefault().post(new SelectedStageEvent(roundOrder.get_id()));
                break;
            }
        }
    }

    @Override
    public void onDataRemoved(String collectionName, String documentID) {
        if (collectionName.equals("rounds") || collectionName.equals("obstacles") ){
            ItemToDelete item = new ItemToDelete(documentID);
            EventBus.getDefault().post(item);
        }else if (collectionName.equals("roundsorder")) {
            if (roundorderList == null) {
                roundorderList = new ArrayList<>();
            }
            for (int i = 0; i < roundorderList.size(); i++) {
                RoundOrder roundorder = roundorderList.get(i);
                if (roundorder.get_id().equals(documentID)) {
                    Log.d(TAG, "onDataRemoved: im sorry!");
                    roundorderList.remove(i);
                    ItemToDelete item = new ItemToDelete(documentID);
                    EventBus.getDefault().post(item);
                    break;
                }
            }
        }
    }

    @Override
    public void performReset() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }




    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements AdapterClickDelegate {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private FragmentCalls fragmentCalls;
        private int tabId;
        private RecyclerView recyclerView;
        private RecyclerAdapter adapter;
        private ItemTouchHelper.SimpleCallback simpleItemTouchCallback;

        public PlaceholderFragment() {

        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        private void requestChangeRoundOrder(int stage) {
            JSONArray params = new JSONArray();
            params.put(Config.GameStatus);

            ArrayList<RecyclerItem> list = adapter.getDataArray();
            for (int i = 0; i < list.size(); i++) {
                params.put(list.get(i).get_id());
            }
            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.CHANGE_ORDER,params,new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                }
            });
            MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "change_round_order");

        }







        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            fragmentCalls = (FragmentCalls) context;
        }

        @Override
        public void onDetach() {
            super.onDetach();
            fragmentCalls = null;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle bundle = getArguments();
            if (bundle != null){
                tabId =  bundle.getInt(ARG_SECTION_NUMBER,-1);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_refresh);
            recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter = new RecyclerAdapter(this,tabId);
            recyclerView.setAdapter(adapter);

            if ((Config.AdminStatus == 0) && tabId == 1) {
                simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    private int fromMovePos;
                    private int toMovePos;
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        if (Config.Stage.getStageType() ==1) return false;

                        fromMovePos = viewHolder.getAdapterPosition();
                        toMovePos = target.getAdapterPosition();
                        Log.d(TAG, "onMove: item moved");
                        return true;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        if (Config.Stage.getStageType() ==1) return;
                        if (viewHolder instanceof RecyclerAdapter.ViewHolder) {
                            RecyclerAdapter.ViewHolder realViewHolder = (RecyclerAdapter.ViewHolder) viewHolder;
                            onCardViewLongClick(1, realViewHolder.get_id());
                        }
                        Log.d(TAG, "onSwiped: item deleted");
                    }


                    @Override
                    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                        Log.d(TAG, "clearView: "+fromMovePos+","+toMovePos);

                        super.clearView(recyclerView, viewHolder);
                        if (fromMovePos < toMovePos) {
                            for (int i = fromMovePos; i < toMovePos; i++) {
                                Collections.swap(adapter.getDataArray(), i, i + 1);
                            }
                        } else {
                            for (int i = fromMovePos; i > toMovePos; i--) {
                                Collections.swap(adapter.getDataArray(), i, i - 1);
                            }
                        }
                        recyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyItemMoved(fromMovePos, toMovePos);
                                requestChangeRoundOrder(1);
                            }
                        });

                    }
                };
                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
                itemTouchHelper.attachToRecyclerView(recyclerView);
            }



            if ((Config.AdminStatus == 0) && tabId == 3) {
                simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        if (viewHolder instanceof RecyclerAdapter.ViewHolder) {
                            RecyclerAdapter.ViewHolder realViewHolder = (RecyclerAdapter.ViewHolder) viewHolder;
                            onCardViewLongClick(3, realViewHolder.get_id());

                        }
                        Log.d(TAG, "onSwiped: item deleted");
                    }


                    @Override
                    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                    }
                };
                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
                itemTouchHelper.attachToRecyclerView(recyclerView);
            }




            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    fragmentCalls.initiateRefresh(tabId);
                    swipeRefreshLayout.setRefreshing(false);
                    notifyAdapterToDataSetChange();
                }
            });
            return rootView;
        }

        public void setData(JSONArray response,int id) {
            adapter.setData(response,id);
        }

        @Override
        public void onCardViewClick(int adapterId, RecyclerItem recyclerItem) {
            if (fragmentCalls != null){
                fragmentCalls.onCardViewClick(adapterId,recyclerItem);
            }
        }

        @Override
        public void onCardViewLongClick(int type, final String objectId) {
            if (fragmentCalls != null){
                fragmentCalls.onCardViewLongClick(type,objectId);
            }
        }

        public void setRoundStart(String id, int time) {
            adapter.setRoundStart(id,time);
        }

        public void notifyAdapterToDataSetChange() {
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<Fragment> fragmentList;
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (fragmentList == null){
                fragmentList = new ArrayList<>();
            }
            if (fragmentList.size()>position){
                return fragmentList.get(position);
            }else{
                return PlaceholderFragment.newInstance(position);
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Groups";
                case 1:
                    return "Rounds";
                case 2:
                    return "Obstacles";
                case 3:
                    return "Stages";
            }
            return null;
        }
    }
}
