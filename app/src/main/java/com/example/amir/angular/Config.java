package com.example.amir.angular;

import java.util.ArrayList;

/**
 * Created by Amir on 12/30/2016.
 */


    public class Config {

        public static final String DELETE_ROUNDS = "/android/deleteround";
        public static final String DELETE_GROUPS = "/android/deletegroup";
        public static final String DELETE_OBSTACLE = "/android/deleteobstacle";

        public static final String ADD_NEW_ROUND = "/android/addround";
        public static final String ADD_NEW_GROUP = "/android/addgroup";
        public static final String START_ROUND = "/android/startround";
        public static final String ADD_POINTS = "/android/addpoints";
        public static final String ADD_OBSTACLE = "/android/addobstacle";
        public static final String UPDATE_OBSTACLES = "/android/updateobstacles";
        public static final String UPDATE_ROUND = "/android/updateround";
        public static final String GET_PLAYOFFS = "/android/setgamestage";
        public static final String SHOW_WINNERS = "/android/show_winners";
        public static final String APPROVE_ROUND = "/android/approve_round";
        public static final String CALCULATE_PLAYOFFS = "/android/calculateplayoffs";
        public static final String CHANGE_ORDER = "/android/orderchange";
        public static final String DELETE_STAGE = "/android/deletestage";
        public static final String ADD_NEW_STAGE = "/android/addstage";
        public static final String EDIT_STAGE = "/android/editstage";
        public static final String RESET_ROUND = "/android/resetround";
        public static final String ADD_RAW_POINTS = "/android/addrawpoints";
        public static final String CREATE_BACKUP = "/android/create_backup";
        public static final String RESTORE_BACKUP = "/android/restore_backup";
        public static final String RESTORE_FROM_DEVICE = "/android/restore_backup_device";
        public static final String REVIEW_ROUND = "/android/reviewround";
        public static final String RESET_GROUP_POINTS = "/android/reset_group_points";
        public static final String RESET_SEASON = "/android/reset_season";


        public static String LOCAL_URL = "http://10.100.102.7:3000";
        public static String REMOTE_URL = "http://138.68.113.44";

        //public static String REMOTE_URL = "https://mona-2366.nodechef.com";
        //public static String SERVER_URL = "http://moona.myvnc.com:3000";
        //public static String SERVER_URL = "https://mona-2366.nodechef.com";

        public static String SERVER_URL = "http://138.68.113.44";

        public static String GET_GROUPS = "/android/getgroups";
        public static String GET_ROUNDS = "/android/getrounds";
        public static String GET_OBSTACLES = "/android/getobstacles";
        public static ArrayList<RecyclerItem> groupsList;
        public static ArrayList<RecyclerItem> roundsList;
        public static ArrayList<RecyclerItem> obstacleList;
        public static int AdminStatus = 3;
        public static int GameStatus = 0;
        public static Integer GameStage1DefaultDuration = 7;
        public static Integer GameStage2DefaultDuration = 5;
        public static Integer GameStage3DefaultDuration = 4;
        public static RoundOrder Stage = null;
        public static long serverDifTime = 0;
}


