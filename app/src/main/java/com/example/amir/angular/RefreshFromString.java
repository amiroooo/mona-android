package com.example.amir.angular;

import org.json.JSONArray;

/**
 * Created by Amir on 12/31/2016.
 */
public interface RefreshFromString {
    void onStringDataRecieved(JSONArray data, int type);
    void requestNewRoundData();
}
