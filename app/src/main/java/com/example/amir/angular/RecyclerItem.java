package com.example.amir.angular;

/**
 * Created by Amir on 12/31/2016.
 */

public abstract class RecyclerItem implements Comparable<RecyclerItem>{
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    private String _id;

    @Override
    public int compareTo(RecyclerItem o) {
        if (o instanceof GroupItem){
            if (((GroupItem)this).getGroupPoints() > ((GroupItem)o).getGroupPoints()){
                return -1;
            }
            if (((GroupItem)this).getGroupPoints() < ((GroupItem)o).getGroupPoints()){
                return 1;
            }
        }
        if (o instanceof RoundItem){
            if (((RoundItem)this).getRoundTime() < ((RoundItem)o).getRoundTime()){
                return -1;
            }
            if (((RoundItem)this).getRoundTime() > ((RoundItem)o).getRoundTime()){
                return 1;
            }
        }
        if (o instanceof RoundOrder){
            if (((RoundOrder)this).getStage()< ((RoundOrder)o).getStage()){
                return -1;
            }
            if (((RoundOrder)this).getStage() > ((RoundOrder)o).getStage()){
                return 1;
            }
        }
        return 0;
    }
    abstract String getOfficialName();
}
