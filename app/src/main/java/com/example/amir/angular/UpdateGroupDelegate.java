package com.example.amir.angular;

import org.json.JSONObject;

/**
 * Created by Amir on 1/25/2017.
 */
public class UpdateGroupDelegate {
    public String get_id() {
        return _id;
    }

    public JSONObject getChangedData() {
        return changedData;
    }

    private String _id;
    private JSONObject changedData;

    public UpdateGroupDelegate(String documentID, JSONObject jsonObject) {
        _id = documentID;
        changedData = jsonObject;
    }
}
