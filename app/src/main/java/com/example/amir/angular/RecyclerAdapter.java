package com.example.amir.angular;

import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Amir on 12/31/2016.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> implements ItemTouchHelperAdapter {

    private final AdapterClickDelegate adapterClickDelegate;

    public ArrayList<RecyclerItem> getDataArray() {
        return dataArray;
    }

    private ArrayList<RecyclerItem> dataArray;
    private HashMap<String,RecyclerItem> totalRoundArray;
    private int type;
    private Handler handler;

    public RecyclerAdapter(AdapterClickDelegate adapterClickDelegate, int type) {
        this.adapterClickDelegate = adapterClickDelegate;
        this.dataArray = new ArrayList<>();
        this.totalRoundArray = new HashMap<>();
        this.type = type;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void notifyChangeInRoundItem(UpdateRoundDelegate event) {
        if (type != 1) return;
            RoundItem roundItem = (RoundItem)totalRoundArray.get(event.getDocumentID());
            if (roundItem != null){
                roundItem.updateByJson(event.getUpdatedValuesJson());
            }
        executeRefreshAdapterData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void notifyChangeInGroupItem(UpdateGroupDelegate event) {
        if (type != 0) return;
        for (int i=0; i<dataArray.size();i++){
            if (event.get_id().equals(dataArray.get(i).get_id())) {
                GroupItem groupItem = (GroupItem) dataArray.get(i);
                groupItem.updateByJson(event.getChangedData());
                break;
            }
        }
        executeRefreshAdapterData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWrongRequestUpdatePoints(FindRoundByIdAndShowPerma event) {
        if (type == 1) {
            RoundItem roundItem = (RoundItem)totalRoundArray.get(event.get_id());
            if (roundItem != null){
                EventBus.getDefault().post(new PermaRoundFragmentDelegate(roundItem));
            }
        }
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public synchronized void notifyChangeInRoundSequence(RoundOrder event) {
        if (type == 0||type == 2) return;
        if (type == 1) {
            if (dataArray == null) return;
            if (event.getStage() != Config.GameStatus) return;


            dataArray.clear();
            JSONArray jsonArray = event.getList();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    String id = (String) jsonArray.get(i);
                    RoundItem roundItem = (RoundItem) totalRoundArray.get(id);
                    if (roundItem != null) {
                        dataArray.add(roundItem);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            executeRefreshAdapterData();
        }else if (type == 3) {
            for (int i = 0; i < dataArray.size(); i++) {
                if (dataArray.get(i) instanceof RoundOrder){
                    RoundOrder roundOrder = (RoundOrder)dataArray.get(i);
                    if (roundOrder.get_id().equals(event.get_id())){
                        dataArray.remove(roundOrder);
                        break;
                    }
                }
            }
            //if (!this.dataArray.contains(event)) {
            this.dataArray.add(event);
            Collections.sort(this.dataArray);
            //}
            executeRefreshAdapterData();
        }
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public synchronized void NotifyChandeDataOnRounds(GameStateChangeDelegate event) {
        executeRefreshAdapterData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void removeAllRounds(DeleteAllRounds event) {
        if (this.dataArray != null && type == 1) {
            this.dataArray = new ArrayList<>();
            this.totalRoundArray = new HashMap<>();
            executeRefreshAdapterData();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void removeAllData(DeleteAllData event) {
        if (this.dataArray != null) {
            this.dataArray = new ArrayList<>();
            this.totalRoundArray = new HashMap<>();
            executeRefreshAdapterData();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void removeItem(ItemToDelete event) {
        if (this.dataArray != null) {
            int pos =-1;
            for (RecyclerItem recyclerItem : this.dataArray) {
                pos += 1;
                if (recyclerItem instanceof RoundItem) {
                    if ((recyclerItem.get_id()).equals(event.getDocumentId())) {
                        this.dataArray.remove(recyclerItem);
                        if (this.totalRoundArray.get(recyclerItem.get_id()) != null) {
                            this.totalRoundArray.remove(recyclerItem.get_id());
                        }
                        //Collections.sort(this.dataArray);
                        executeRefreshAdapterData();
                        break;
                    }
                } else {
                    if ((recyclerItem.get_id()).equals(event.getDocumentId())) {
                        //onItemDismiss(pos);
                        this.dataArray.remove(recyclerItem);
                        executeRefreshAdapterData();
                        break;
                    }
                }
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public synchronized void addRound(RoundItem event) {
        if (this.dataArray != null && type == 1) {
            this.totalRoundArray.put(event.get_id(),event);
        }
    }

    public void executeRefreshAdapterData(){
        if (handler == null){
            handler =  new Handler();
        }
        handler.removeCallbacksAndMessages(null);
        Runnable myRunnable = new Runnable() {
            public void run() {
                notifyDataSetChanged();
            }
        };
        handler.postDelayed(myRunnable,1000);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void addGroup(GroupItem event) {
        if (this.dataArray != null && type == 0) {
            removeItem(new ItemToDelete(event.get_id()));
            this.dataArray.add(event);
            Collections.sort(this.dataArray);
            executeRefreshAdapterData();
            if (type == 0) {
                Config.groupsList = this.dataArray;
            } else if (type == 1) {
                Config.roundsList = this.dataArray;
            } else if (type == 2) {
                Config.obstacleList = this.dataArray;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void addObstacle(ObstacleItem event) {
        if (this.dataArray != null && type == 2) {
            removeItem(new ItemToDelete(event.get_id()));
            this.dataArray.add(event);
            Collections.sort(this.dataArray);
            executeRefreshAdapterData();
            if (type == 0) {
                Config.groupsList = this.dataArray;
            } else if (type == 1) {
                Config.roundsList = this.dataArray;
            } else if (type == 2) {
                Config.obstacleList = this.dataArray;
            }
        }
    }


    public void setData(JSONArray dataArray, int type) {
        if (this.dataArray != null && type == 1) {
            for (RecyclerItem recyclerItem : this.dataArray) {
                if (recyclerItem instanceof RoundItem) {
                    RoundItem roundItem = (RoundItem) recyclerItem;
                    if (EventBus.getDefault().isRegistered(roundItem)) {
                        EventBus.getDefault().unregister(roundItem);
                    }
                }
            }
        }
        if (type != 1) {
            this.dataArray = new ArrayList<>();
        }
        this.totalRoundArray = new HashMap<>();


        for (int i = 0; i < dataArray.length(); i++) {
            JSONObject item;
            try {
                item = (JSONObject) dataArray.get(i);
                if (type == 0) {
                    GroupItem recyclerItem = new GroupItem();
                    recyclerItem.set_id(item.getString("_id"));
                    recyclerItem.setGroupName(item.getString("groupName"));
                    recyclerItem.setGroupPoints(item.getInt("points"));
                    try {
                        recyclerItem.setDescription(item.getString("description"));
                    } catch (Throwable e) {
                        recyclerItem.setDescription("");
                    }
                    this.dataArray.add(recyclerItem);
                } else if (type == 1) {
                    RoundItem recyclerItem = new RoundItem();
                    recyclerItem.set_id(item.getString("_id"));
                    recyclerItem.setGroup1(item.getString("leftSide"));
                    recyclerItem.setGroup2(item.getString("rightSide"));
                    recyclerItem.setIsDone(item.getInt("isDone"));
                    recyclerItem.setRoundStage(item.getInt("stage"));
                    try {
                        recyclerItem.setRoundTime(item.getInt("timeLeft"));
                    } catch (Throwable e) {
                        recyclerItem.setRoundTime(Config.GameStage1DefaultDuration);
                    }
                    try {
                        recyclerItem.setObstacleItem1Name(item.getString("obstacle1"));
                    } catch (Throwable e) {
                        recyclerItem.setObstacleItem1Name("none");
                    }
                    try {
                        recyclerItem.setObstacleItem2Name(item.getString("obstacle2"));
                    } catch (Throwable e) {
                        recyclerItem.setObstacleItem2Name("none");
                    }
                    try {
                        recyclerItem.setObstacleItem3Name(item.getString("obstacle3"));
                    } catch (Throwable e) {
                        recyclerItem.setObstacleItem3Name("none");
                    }
                    try {
                        recyclerItem.setObstacleItem4Name(item.getString("obstacle4"));
                    } catch (Throwable e) {
                        recyclerItem.setObstacleItem4Name("none");
                    }

                    try{
                        recyclerItem.setObs1Points(item.getInt("obs1Points"));
                    }catch(JSONException e){
                        recyclerItem.setObs1Points(0);
                    }
                    try{
                        recyclerItem.setObs2Points(item.getInt("obs2Points"));
                    }catch(JSONException e){
                        recyclerItem.setObs2Points(0);

                    }
                    try{
                        recyclerItem.setObs3Points(item.getInt("obs3Points"));
                    }catch(JSONException e){
                        recyclerItem.setObs3Points(0);
                    }
                    try{
                        recyclerItem.setObs4Points(item.getInt("obs4Points"));
                    }catch(JSONException e){
                        recyclerItem.setObs4Points(0);
                    }


                    try{
                        recyclerItem.setLeftBonusPoints(item.getInt("leftBonus"));
                    }catch(JSONException e){
                        recyclerItem.setLeftBonusPoints(0);
                    }

                    try{
                        recyclerItem.setRightBonusPoints(item.getInt("rightBonus"));
                    }catch(JSONException e){
                        recyclerItem.setRightBonusPoints(0);
                    }


                    this.totalRoundArray.put(recyclerItem.get_id(),recyclerItem);
                    //if (recyclerItem.getRoundStage() == Config.GameStatus) {
                    //    this.dataArray.add(recyclerItem);
                    //}
                } else if (type == 2) {
                    ObstacleItem recyclerItem = new ObstacleItem();
                    recyclerItem.set_id(item.getString("_id"));
                    recyclerItem.setName(item.getString("name"));
                    recyclerItem.setDescription(item.getString("description"));
                    recyclerItem.setOwner(item.getString("owner"));
                    recyclerItem.setMaxPoints(item.getString("maxPoints"));
                    recyclerItem.setTickPoints(item.getString("tickPoints"));
                    this.dataArray.add(recyclerItem);

                }else if (type == 3) {
                    int stage = item.getInt("stage");
                    String name = item.getString("name");
                    JSONArray order = item.getJSONArray("order");
                    RoundOrder roundOrder = new RoundOrder(order,item.getString("_id"),stage);
                    roundOrder.setName(name);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (type == 0) {
            Config.groupsList = this.dataArray;
        } else if (type == 1) {
            Config.roundsList = this.dataArray;
        } else if (type == 2) {
            Config.obstacleList = this.dataArray;
        }
        Collections.sort(this.dataArray);
        executeRefreshAdapterData();
    }

    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        if (!EventBus.getDefault().isRegistered(holder)) {
            EventBus.getDefault().register(holder);
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (EventBus.getDefault().isRegistered(holder)) {
            EventBus.getDefault().unregister(holder);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (type == 0) {
            View rowView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_row, parent, false);
            return new GroupsViewHolder(rowView);
        } else if (type == 1) {
            View rowView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.rounds_row, parent, false);
            return new RoundsViewHolder(rowView);
        } else if (type == 2) {
            View rowView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.obstacles_row, parent, false);
            return new ObstaclesViewHolder(rowView);
        }else {
            View rowView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.round_order_row, parent, false);
            return new RoundsOrderViewHolder(rowView);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.set_id(dataArray.get(position).get_id());
        if (type == 0) {
            final GroupItem groupItem = (GroupItem) dataArray.get(position);
            if (holder instanceof GroupsViewHolder) {
                GroupsViewHolder groupsViewHolder = (GroupsViewHolder) holder;
                holder.setTextView1(groupItem.getGroupName());
                holder.setTextView2(Integer.toString(groupItem.getGroupPoints()));
                groupsViewHolder.getTextView3().setText(groupItem.getDescription());
                holder.getCardView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (adapterClickDelegate != null) {
                            adapterClickDelegate.onCardViewClick(type, groupItem);
                        }
                    }
                });
            }
            if (position<4) {
                holder.setStateAppearence(10);
            }else
                holder.setStateAppearence(0);
            holder.getCardView().setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if ((Config.AdminStatus == 0) && adapterClickDelegate != null) {
                        adapterClickDelegate.onCardViewLongClick(type, groupItem.get_id());
                    }
                    return true;
                }
            });
        } else if (type == 1) {
            String roundItemId = dataArray.get(position).get_id();
            RoundItem roundItem = (RoundItem)totalRoundArray.get(roundItemId);

            if (roundItem == null){
                executeRefreshAdapterData();
                return;
            }
            holder.setIsRecyclable(false);
            if (holder instanceof RoundsViewHolder) {
                RoundsViewHolder roundsViewHolder = (RoundsViewHolder) holder;
                Integer left = roundItem.getObs1Points()+roundItem.getObs2Points()+roundItem.getLeftBonusPoints();
                Integer right = roundItem.getObs3Points()+roundItem.getObs4Points()+roundItem.getRightBonusPoints();
                roundsViewHolder.getLeftPointsView().setText(left.toString());
                roundsViewHolder.getRightPointsView().setText(right.toString());
                if (Config.Stage != null){
                    if (Config.Stage.getStageType()==1){
                        int roundOrderplace = Config.Stage.getRoundQueueOrder(roundItem.get_id());
                        if (roundOrderplace >= 0){
                            if (roundOrderplace < 4) {
                                roundsViewHolder.setRoundPlace("Quarter final round.");
                            }else if (roundOrderplace < 6) {
                                roundsViewHolder.setRoundPlace("Half final round.");
                            }else if (roundOrderplace == 6) {
                                roundsViewHolder.setRoundPlace("Battle for third place.");
                            }else if (roundOrderplace == 7) {
                                roundsViewHolder.setRoundPlace("Final Round.");
                            }
                        }
                    }else{
                        roundsViewHolder.setRoundPlace("Stage " + Config.Stage.getStage().toString() + " round.");
                    }
                }
            }
            holder.setTextView1(roundItem.getGroup1());
            holder.setTextView2(roundItem.getGroup2());
            holder.setStateAppearence(roundItem.getIsDone());

            final RoundItem finalRoundItem = roundItem;
            holder.getCardView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adapterClickDelegate != null) {
                        adapterClickDelegate.onCardViewClick(type, finalRoundItem);
                    }
                }
            });

        } else if (type == 2){
            final ObstacleItem obstacleItem = (ObstacleItem) dataArray.get(position);
            if (holder instanceof ObstaclesViewHolder) {
                ObstaclesViewHolder obstaclesViewHolder = (ObstaclesViewHolder) holder;
                obstaclesViewHolder.getNameView().setText(obstacleItem.getName());
                obstaclesViewHolder.getDescriptionView().setText(obstacleItem.getDescription());
                obstaclesViewHolder.getOwnerView().setText(obstacleItem.getOwner());
                obstaclesViewHolder.getTickView().setText(obstacleItem.getTickPoints());
                obstaclesViewHolder.getMax_pointsView().setText(obstacleItem.getMaxPoints());
            }
            holder.getCardView().setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if ((Config.AdminStatus == 0) && adapterClickDelegate != null) {
                        adapterClickDelegate.onCardViewLongClick(type, obstacleItem.get_id());
                    }
                    return true;
                }
            });
        }else if (type == 3){
            holder.setIsRecyclable(false);
            final RoundOrder roundOrder = (RoundOrder) dataArray.get(position);
            final RoundsOrderViewHolder roundOrderViewholder = (RoundsOrderViewHolder)holder;
            if (roundOrder.getStageType() == 0){
                roundOrderViewholder.getTypeView().setText("Normal");
            }else{
                roundOrderViewholder.getTypeView().setText("Playoff");
            }
            roundOrderViewholder.getDurationView().setText(roundOrder.getRoundTime().toString());
            if (holder instanceof RoundsOrderViewHolder) {
                RoundsOrderViewHolder roundsOrderViewHolder = (RoundsOrderViewHolder) holder;
                roundsOrderViewHolder.getNameView().setText(roundOrder.getName());
                roundsOrderViewHolder.getStageView().setText(roundOrder.getStage().toString());

                if (roundOrder.getStage()== Config.GameStatus){
                    roundsOrderViewHolder.setBgColorBySelection(true);
                }else
                    roundsOrderViewHolder.setBgColorBySelection(false);


            }
            holder.getCardView().setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (Config.AdminStatus==0)
                        EventBus.getDefault().post(new EditRoundOrder(roundOrder));

                    return true;
                }
            });
            holder.getCardView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((Config.AdminStatus==0) && (adapterClickDelegate != null)) {
                        adapterClickDelegate.onCardViewClick(type, roundOrder);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (dataArray == null) return 0;
        return dataArray.size();
    }

    public void setRoundStart(String id, int time) {
        for (RecyclerItem recyclerItem : dataArray) {
            if (recyclerItem instanceof RoundItem) {
                RoundItem roundItem = (RoundItem) recyclerItem;
                if ((roundItem.get_id()).equals(id)) {
                    roundItem.setIsDone(2);
                    roundItem.setMatchEnd(time);
                }
            }
        }
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDismiss(int position) {
        this.dataArray.remove(position);

        //mRecyclerView.removeViewAt(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public void setTextView2(String textView2) {
            TextView2.setText(textView2);
        }

        public void setTextView1(String textView1) {
            TextView1.setText(textView1);
        }

        public CardView getCardView() {
            return cardView;
        }

        protected AutofitTextView TextView1;
        protected AutofitTextView TextView2;
        protected CardView cardView;

        public String get_id() {
            return _id;
        }

        protected String _id;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void setStateAppearence(int i) {
            if (i == 4) {
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimaryDone));
            } else if (i == 3 || i == 2) {
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimaryOngoing));
            } else if (i == 1) {
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimaryReady));
            }else if (i == 10) {
                 cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPassGroups));
            } else {
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimary));
            }
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void setStatus(RoundStatusEvent event) {
            if ((event.get_id()).equals(this._id)) {
                setStateAppearence(event.getStatus());
                RoundItem roundItem = (RoundItem)totalRoundArray.get(event.get_id());
                if (roundItem != null){
                    roundItem.setIsDone(event.getStatus());

                    if (event.getStatus()>1 && event.getStatus()<4){
                        Log.d("Adapter", "setStatus: trying to open frag");
                        PermaRoundFragmentDelegate t = new PermaRoundFragmentDelegate(roundItem);
                        EventBus.getDefault().post(t);
                    }

                }
            }
        }

        public void set_id(String id) {
            this._id = id;
        }
    }


    class GroupsViewHolder extends ViewHolder {
        public TextView getTextView3() {
            return TextView3;
        }

        private final TextView TextView3;

        public void setTextView2(String textView2) {
            TextView2.setText(textView2);
        }

        public void setTextView1(String textView1) {
            TextView1.setText(textView1);
        }

        public GroupsViewHolder(View itemView) {
            super(itemView);
            TextView1 = (AutofitTextView) itemView.findViewById(R.id.group_name);
            TextView2 = (AutofitTextView) itemView.findViewById(R.id.group_points);
            cardView = (CardView) itemView.findViewById(R.id.card_view1);
            TextView3 = (TextView) itemView.findViewById(R.id.group_description);
        }


    }

    class RoundsViewHolder extends ViewHolder {
        private final TextView leftPointsView;
        private final TextView rightPointsView;
        private final AutofitTextView roundPlace;

        public void setTextView2(String textView2) {
            TextView2.setText(textView2);
        }

        public void setTextView1(String textView1) {
            TextView1.setText(textView1);
        }

        public RoundsViewHolder(View itemView) {
            super(itemView);
            TextView1 = (AutofitTextView) itemView.findViewById(R.id.group_1);
            TextView2 = (AutofitTextView) itemView.findViewById(R.id.group_2);
            cardView = (CardView) itemView.findViewById(R.id.card_view2);
            leftPointsView = (TextView) itemView.findViewById(R.id.left_points);
            rightPointsView = (TextView) itemView.findViewById(R.id.right_points);
            roundPlace = (AutofitTextView) itemView.findViewById(R.id.round_place);

        }

        public void setRoundPlace(String text){
            roundPlace.setText(text);
        }


        public TextView getLeftPointsView() {
            return leftPointsView;
        }

        public TextView getRightPointsView() {
            return rightPointsView;
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void setPoints(PointsStatusEvent event){
            if ((event.get_id()).equals(get_id())){

                Integer left = event.getLeftobs1()+event.getLeftobs2()+event.getLeftBonus();
                Integer right = event.getRightobs1()+event.getRightobs2()+event.getRightBonus();

                Log.d("roundviewholder", "setPoints: found set point for the changed viewholder "+ left +" , "+ right);
                leftPointsView.setText(left.toString());
                rightPointsView.setText(right.toString());
            }
        }


    }

    class ObstaclesViewHolder extends ViewHolder {
        private final TextView nameView;

        public TextView getNameView() {
            return nameView;
        }

        public TextView getDescriptionView() {
            return descriptionView;
        }

        public TextView getOwnerView() {
            return ownerView;
        }

        public TextView getMax_pointsView() {
            return max_pointsView;
        }

        public TextView getTickView() {
            return tickView;
        }

        private final TextView descriptionView;
        private final AutofitTextView ownerView;
        private final TextView max_pointsView;
        private final TextView tickView;

        public ObstaclesViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view3);
            nameView = (TextView) itemView.findViewById(R.id.name);
            descriptionView = (TextView) itemView.findViewById(R.id.description);
            ownerView = (AutofitTextView) itemView.findViewById(R.id.owner);
            max_pointsView = (TextView) itemView.findViewById(R.id.max_points);
            tickView = (TextView) itemView.findViewById(R.id.tick);
        }

    }


    class RoundsOrderViewHolder extends ViewHolder {
        private final TextView nameView;

        public TextView getNameView() {
            return nameView;
        }

        public TextView getDurationView() {
            return durationView;
        }

        public TextView getOwnerView() {
            return ownerView;
        }

        public TextView getStageView() {
            return stageView;
        }

        public TextView getTypeView() {
            return typeView;
        }

        private final TextView durationView;
        private final TextView ownerView;
        private final TextView stageView;
        private final TextView typeView;

        /*
        @Subscribe(threadMode = ThreadMode.MAIN)
        public void notifyChangeInRoundItem(UpdateRoundDelegate event) {
            if (event.getDocumentID().equals(get_id())){
                roundItem.updateByJson(event.getUpdatedValuesJson());

            }
        }
        */

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void setSelectedStage(SelectedStageEvent event){
            if ((event.get_id()).equals(get_id())){
                setBgColorBySelection(true);
            }else
                setBgColorBySelection(false);
        }

        public void setBgColorBySelection(boolean isSelected){
            if (isSelected){
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimaryReady));
            }else
                cardView.setCardBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.colorPrimary));
        }



        public RoundsOrderViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view3);
            nameView = (TextView) itemView.findViewById(R.id.name);
            durationView = (TextView) itemView.findViewById(R.id.round_duration);
            ownerView = (TextView) itemView.findViewById(R.id.owner);
            stageView = (TextView) itemView.findViewById(R.id.max_points);
            typeView = (TextView) itemView.findViewById(R.id.stage_type);

        }

    }

}
interface ItemTouchHelperAdapter {

    void onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
