package com.example.amir.angular;

/**
 * Created by Amir on 1/8/2017.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.github.florent37.singledateandtimepicker.dialog.DoubleDateAndTimePickerDialog;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Amir on 11/19/2016.
 */

public class CreateNewRound extends BottomSheetDialog implements View.OnClickListener{


    private Spinner team1Editor;
    private Spinner team2Editor;
    private Button team3Editor;
    private EditText roundDuration;
    private Context context;
    private Date realDate;
    private SingleDateAndTimePickerDialog.Builder singleDateAndTimePickerDialog;

    public CreateNewRound(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public void hide() {
        if (singleDateAndTimePickerDialog != null){
            singleDateAndTimePickerDialog.close();
            singleDateAndTimePickerDialog = null;
        }
        super.hide();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = LayoutInflater.from(getContext()).inflate(R.layout.new_round_sheet,null);
        setContentView(root);
        setCancelable(false);
        team1Editor = (Spinner)root.findViewById(R.id.editVideoId);
        team2Editor = (Spinner)root.findViewById(R.id.editVideoId2);

        team3Editor = (Button)root.findViewById(R.id.editVideoId3);
        Button confirmButton = (Button) root.findViewById(R.id.button_confirm);
        Button cancelButton = (Button) root.findViewById(R.id.button_cancel);
        roundDuration = (EditText) root.findViewById(R.id.round_duration);



        team1Editor.setAdapter( getAdapterForSpinner(Config.groupsList));
        team2Editor.setAdapter( getAdapterForSpinner(Config.groupsList));
        team2Editor.setSelection(1);
        roundDuration.setText(Config.Stage.getRoundTime().toString());
        team3Editor.setOnClickListener(this);
        confirmButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (singleDateAndTimePickerDialog != null){
            singleDateAndTimePickerDialog.close();
            singleDateAndTimePickerDialog = null;
        }
        cancel();
    }


    private ArrayAdapter<String> getAdapterForSpinner(ArrayList<RecyclerItem> recyclerItems){
        ArrayList<String> stringList = new ArrayList<>();
        if (recyclerItems != null) {
            for (RecyclerItem recyclerItem : recyclerItems) {
                String testString = recyclerItem.getOfficialName();
                if (testString != null) {
                    stringList.add(recyclerItem.getOfficialName());
                }
            }
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, stringList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        return spinnerArrayAdapter;
    }




    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.button_confirm){
            if (realDate == null){
                //Toast.makeText(context,"Must pick Date first!",Toast.LENGTH_SHORT).show();
                //return;
                realDate = new Date();
            }
            JSONArray params = new JSONArray();
            params.put(team1Editor.getSelectedItem().toString());
            params.put(team2Editor.getSelectedItem().toString());
            //long tempTime = realDate.getTime();

            //int size;
            //if (Config.roundsList == null){
            //    size=0;
            //}else
            //    size=Config.roundsList.size();

            params.put(-1);

            Log.d("ROUND", "onClick: game status on android: "+Config.GameStatus);
            Integer t;
            try {
                t = Integer.parseInt(roundDuration.getText().toString());
                Log.d("fragment", "onClick: added round as time indicated");
            }catch(NumberFormatException e) {
                if (Config.Stage != null){
                    t = Config.Stage.getRoundTime();
                }
                else
                    t=1;
            }
            params.put(t);
            params.put(Config.GameStatus);
            JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.POST, Config.SERVER_URL+Config.ADD_NEW_ROUND,params,new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    if (context != null) {
                        //((RefreshFromString) context).onStringDataRecieved(response, 1);
                        context = null;
                        cancel();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Amir", "add new video response Error: " + error.getMessage());
                }
            });
            MonaAdminApp.getInstance().addToRequestQueue(stringRequest, "add_new_round");

        }

        if (view.getId() == R.id.button_cancel){
            cancel();
        }

        if (view.getId() == R.id.editVideoId3){
            SingleDateAndTimePickerDialog.Builder singleDateAndTimePickerDialog = new SingleDateAndTimePickerDialog.Builder(context).title("Choose time round starts at:").mustBeOnFuture().listener(new SingleDateAndTimePickerDialog.Listener() {
                @Override
                public void onDateSelected(Date date) {
                    realDate = date;
                    team3Editor.setText(date.toLocaleString());
                }
            });
            singleDateAndTimePickerDialog.display();
        }




    }
}
