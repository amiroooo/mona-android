package com.example.amir.angular;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Amir on 12/31/2016.
 */

public class GroupItem extends RecyclerItem{
    private String groupName;
    private String description;
    private int groupPoints;

    public int getGroupPoints() {
        return groupPoints;
    }

    public void setGroupPoints(int groupPoints) {
        this.groupPoints = groupPoints;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @Override
    public String getOfficialName() {
        return getGroupName();
    }

    public void updateByJson(JSONObject changedData) {
        if (changedData.has("points")){
            try {
                setGroupPoints(changedData.getInt("points"));
            } catch (JSONException e) {
            }
        }
    }
}
