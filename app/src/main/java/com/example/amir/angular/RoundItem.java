package com.example.amir.angular;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.MainThread;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Amir on 12/31/2016.
 */

public class RoundItem extends RecyclerItem implements Parcelable {

    private Integer roundTime;
    private String Group1;
    private int group1PointsForTheRound;
    private int group2PointsForTheRound;
    private String Group2;
    private int isDone;
    private String obstacleItem1Name;
    private String obstacleItem2Name;
    private String obstacleItem3Name;
    private String obstacleItem4Name;
    private int roundStage;
    private int obs1tick;
    private int obs2tick;
    private int obs3tick;
    private int obs4tick;
    private long matchEnd;

    private Integer obs1Points;
    private Integer obs2Points;
    private Integer obs3Points;
    private Integer obs4Points;

    public Integer getLeftBonusPoints() {
        return leftBonusPoints;
    }

    public void setLeftBonusPoints(Integer leftBonusPoints) {
        this.leftBonusPoints = leftBonusPoints;
    }

    public Integer getRightBonusPoints() {
        return rightBonusPoints;
    }

    public void setRightBonusPoints(Integer rightBonusPoints) {
        this.rightBonusPoints = rightBonusPoints;
    }

    private Integer leftBonusPoints;
    private Integer rightBonusPoints;





    public Integer getObs1Points() {
        return obs1Points;
    }

    public void setObs1Points(Integer obs1Points) {
        this.obs1Points = obs1Points;
    }

    public Integer getObs2Points() {
        return obs2Points;
    }

    public void setObs2Points(Integer obs2Points) {
        this.obs2Points = obs2Points;
    }

    public Integer getObs3Points() {
        return obs3Points;
    }

    public void setObs3Points(Integer obs3Points) {
        this.obs3Points = obs3Points;
    }

    public Integer getObs4Points() {
        return obs4Points;
    }

    public void setObs4Points(Integer obs4Points) {
        this.obs4Points = obs4Points;
    }


    public int getRoundStage() {
        return roundStage;
    }

    public Integer getObs1tick() {
        return obs1tick;
    }

    public void setObs1tick(int obs1tick) {
        this.obs1tick = obs1tick;
    }

    public Integer getObs2tick() {
        return obs2tick;
    }

    public void setObs2tick(int obs2tick) {
        this.obs2tick = obs2tick;
    }


    public long getMatchEnd() {
        return matchEnd+Config.serverDifTime;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setStatus(RoundStatusEvent event){
        if ((event.get_id()).equals(get_id())){
            setIsDone(event.getStatus());
            if (event.getStatus() == 2){
                //setMatchEnd(event.getCounterEnds());
                setObs1tick(event.getObs1tick());
                setObs2tick(event.getObs2tick());
            }
        }
    }


    public String getObstacleItem1Name() {
        return obstacleItem1Name;
    }

    public void setObstacleItem1Name(String obstacleItem1Name) {
        this.obstacleItem1Name = obstacleItem1Name;
    }

    public String getObstacleItem2Name() {
        return obstacleItem2Name;
    }

    public void setObstacleItem2Name(String obstacleItem2Name) {
        this.obstacleItem2Name = obstacleItem2Name;
    }



    public String getObstacleItem3Name() {
        return obstacleItem3Name;
    }

    public void setObstacleItem3Name(String obstacleItem3Name) {
        this.obstacleItem3Name = obstacleItem3Name;
    }

    public String getObstacleItem4Name() {
        return obstacleItem4Name;
    }

    public void setObstacleItem4Name(String obstacleItem4Name) {
        this.obstacleItem4Name = obstacleItem4Name;
    }

    public Integer getObs4tick() {
        return obs4tick;
    }

    public void setObs4tick(int obs4tick) {
        this.obs4tick = obs4tick;
    }

    public Integer getObs3tick() {
        return obs3tick;
    }

    public void setObs3tick(int obs3tick) {
        this.obs3tick = obs3tick;
    }



    public String getGroup2() {
        return Group2;
    }

    public Integer getRoundTime(){
        return this.roundTime;
    }

    public void setRoundTime(Integer roundTime) {
        this.roundTime = roundTime;
    }

    public void setGroup2(String group2) {
        Group2 = group2;
    }

    public String getGroup1() {
        return Group1;
    }

    public void setGroup1(String group1) {
        Group1 = group1;
    }


    public int getGroup1PointsForTheRound() {
        return group1PointsForTheRound;
    }

    public void setGroup1PointsForTheRound(int group1PointsForTheRound) {
        this.group1PointsForTheRound = group1PointsForTheRound;
    }

    public int getGroup2PointsForTheRound() {
        return group2PointsForTheRound;
    }

    public void setGroup2PointsForTheRound(int group2PointsForTheRound) {
        this.group2PointsForTheRound = group2PointsForTheRound;
    }


    public RoundItem() {
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Group1);
        dest.writeString(this.Group2);
        dest.writeString(this.obstacleItem1Name);
        dest.writeString(this.obstacleItem2Name);

        dest.writeInt(this.group1PointsForTheRound);
        dest.writeInt(this.group2PointsForTheRound);
        dest.writeInt(this.roundTime);
        dest.writeInt(this.isDone);
        dest.writeLong(this.matchEnd);

        dest.writeInt(this.obs1Points);
        dest.writeInt(this.obs2Points);
        dest.writeInt(this.obs3Points);
        dest.writeInt(this.obs4Points);

        dest.writeInt(this.leftBonusPoints);
        dest.writeInt(this.rightBonusPoints);


        dest.writeString(get_id());
    }

    protected RoundItem(Parcel in) {
        this.Group1 = in.readString();
        this.Group2 = in.readString();
        this.obstacleItem1Name = in.readString();
        this.obstacleItem2Name = in.readString();
        this.group1PointsForTheRound = in.readInt();
        this.group2PointsForTheRound = in.readInt();
        this.roundTime = in.readInt();
        this.isDone = in.readInt();
        this.matchEnd = in.readLong();
        this.obs1Points = in.readInt();
        this.obs2Points = in.readInt();
        this.obs3Points = in.readInt();
        this.obs4Points = in.readInt();

        this.leftBonusPoints = in.readInt();
        this.rightBonusPoints = in.readInt();

        set_id(in.readString());
    }

    public static final Creator<RoundItem> CREATOR = new Creator<RoundItem>() {
        @Override
        public RoundItem createFromParcel(Parcel source) {
            return new RoundItem(source);
        }

        @Override
        public RoundItem[] newArray(int size) {
            return new RoundItem[size];
        }
    };

    @Override
    public String getOfficialName() {
        return getGroup1()+" : "+getGroup2();
    }

    public int getIsDone() {
        return isDone;
    }

    public void setIsDone(int isDone) {
        this.isDone = isDone;
    }

    public void setMatchEnd(long time) {
        this.matchEnd = time;
    }

    public void setRoundStage(int roundStage) {
        this.roundStage = roundStage;
    }

    public void updateByJson(JSONObject updatedValuesJson) {
        try {
            setGroup1(updatedValuesJson.getString("leftSide"));
        } catch (JSONException e) {
        }
        try {
            setGroup2(updatedValuesJson.getString("rightSide"));
        } catch (JSONException e) {
        }
        try {
            setIsDone(updatedValuesJson.getInt("isDone"));
        } catch (JSONException e) {
        }
        try {
            setRoundStage(updatedValuesJson.getInt("stage"));
        } catch (JSONException e) {
        }

        try{
            setRoundTime(updatedValuesJson.getInt("timeLeft"));
        }catch(JSONException e){

        }

        try{
            setMatchEnd(updatedValuesJson.getInt("executeTime"));
        }catch(JSONException e){

        }

        try{
            setObstacleItem1Name(updatedValuesJson.getString("obstacle1"));
        }catch(JSONException e){
        }
        try{
            setObstacleItem2Name(updatedValuesJson.getString("obstacle2"));
        }catch(JSONException e){

        }
        try{
            setObstacleItem3Name(updatedValuesJson.getString("obstacle3"));
        }catch(JSONException e){

        }
        try{
            setObstacleItem4Name(updatedValuesJson.getString("obstacle4"));
        }catch(JSONException e){

        }
        try{
            setObs1Points(updatedValuesJson.getInt("obs1Points"));
        }catch(JSONException e){

        }
        try{
            setObs2Points(updatedValuesJson.getInt("obs2Points"));
        }catch(JSONException e){

        }
        try{
            setObs3Points(updatedValuesJson.getInt("obs3Points"));
        }catch(JSONException e){

        }
        try{
            setObs4Points(updatedValuesJson.getInt("obs4Points"));
        }catch(JSONException e){

        }

        try{
            setLeftBonusPoints(updatedValuesJson.getInt("leftBonus"));
        }catch(JSONException e){

        }

        try{
            setRightBonusPoints(updatedValuesJson.getInt("rightBonus"));
        }catch(JSONException e){

        }

        // send points update
        Log.d("RoundItem", "round data updated: ");
        PointsStatusEvent t = new PointsStatusEvent();
        t.set_id(get_id());
        t.setLeftobs1(getObs1Points());
        t.setLeftobs2(getObs2Points());
        t.setRightobs1(getObs3Points());
        t.setRightobs2(getObs4Points());

        t.setLeftBonus(getLeftBonusPoints());
        t.setRightBonus(getRightBonusPoints());
        EventBus.getDefault().post(t);

    }

}
